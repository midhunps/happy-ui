import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Global } from './models/global';
import { ClipboardModule } from 'ngx-clipboard';


import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { AppRoutingModule } from './/app-routing.module';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { PostListingComponent } from './pages/post-listing/post-listing.component';
import { NotificationComponent } from './layout/header/notification/notification.component';
import { MainSearchComponent } from './layout/header/main-search/main-search.component';
import { CarouselComponent } from './components/carousel/carousel.component';

import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './layout/footer/footer.component';
import { LeftMenusComponent } from './layout/left-menus/left-menus.component';
import { CreatePostComponent } from './pages/post-listing/create-post/create-post.component';
import { PostComponent } from './pages/post-listing/post/post.component';
import { ProfileViewComponent } from './pages/profile-view/profile-view.component';
import { ModalLikeComponent } from './components/modal-like/modal-like.component';
import { LikedListComponent } from './pages/post-listing/post/liked-list/liked-list.component';
import { CommentItemComponent } from './pages/post-listing/post/comment-item/comment-item.component';
import { DetailedPostComponent } from './pages/post-listing/post/detailed-post/detailed-post.component';
import { HomeComponent } from './layout/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { TermsConditionsComponent } from './components/terms-conditions/terms-conditions.component';
import { ResMenuComponent } from './layout/header/res-menu/res-menu.component';
import { SearchComponent } from './pages/search/search.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ProfileListingComponent } from './components/profile-listing/profile-listing.component';
import { ActivateProfileComponent } from './pages/activate-profile/activate-profile.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CategoryComponent } from './pages/category/category.component';
import { LoaderComponent } from './components/loader/loader.component';
import { MenuItemComponent } from './layout/left-menus/menu-item/menu-item.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { AdvertisementComponent } from './components/advertisement/advertisement.component';
import { TwitterComponent } from './components/twitter/twitter.component';
import { CategoryPostListingComponent } from './layout/category-post-listing/category-post-listing.component';
import { CommentTextComponent } from './pages/post-listing/post/comment-item/comment-text/comment-text.component';
import { PostDetailedComponent } from './pages/post-listing/post-detailed/post-detailed.component';
import { ShareButtonsModule } from 'ngx-sharebuttons';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UserProfileComponent,
    PostListingComponent,
    NotificationComponent,
    MainSearchComponent,
    CarouselComponent,
    FooterComponent,
    LeftMenusComponent,
    CreatePostComponent,
    PostComponent,
    ProfileViewComponent,
    ModalLikeComponent,
    LikedListComponent,
    CommentItemComponent,
    DetailedPostComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    TermsConditionsComponent,
    ResMenuComponent,
    SearchComponent,
    SettingsComponent,
    ProfileListingComponent,
    ActivateProfileComponent,
    ContactComponent,
    CategoryComponent,
    LoaderComponent,
    MenuItemComponent,
    ForgotPasswordComponent,
    AdvertisementComponent,
    TwitterComponent,
    CategoryPostListingComponent,
    CommentTextComponent,
    PostDetailedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ClipboardModule,
    ShareButtonsModule.forRoot()
  ],
  providers: [Global],
  bootstrap: [AppComponent]
})
export class AppModule { }
