import { Component, OnInit, Input, Inject, NgZone, Output, EventEmitter } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { OwnDetails } from '../../../models/own-details';
import { ApiUrls } from '../../../models/api-urls';
import {Global} from '../../../models/global'
import {Notification} from '../../../models/notification'
import {StatusClass} from '../../../models/status-class'
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  static temp:NotificationComponent;
  private NgbModalRef: NgbModalRef;
  errorMessageBox;
  loader;
  errorMessage;
  access_token: string = localStorage.getItem('access_token');
  notification: Notification;
  statusClass: StatusClass;
  notificationList;
  mediaUrl: string = ApiUrls.mediaUrl;
  notificationCount;
  loading = true;
  noNotification = false;

  @Output() notifCountFnOutput = new EventEmitter<any>();

  constructor(private modalService: NgbModal, private http:Http, private zone: NgZone, private router: Router, private global: Global) {
    NotificationComponent.temp = this;
  }

  ngOnInit() {
    this.getNotificcation(0);
    this.callNotification();
  }
  openLg(content, postId, ntId, notifiType) {
    localStorage.setItem('postId', postId);
    this.NgbModalRef = this.modalService.open(content, { size: 'lg' });
    this.readNotification(ntId, notifiType);
  }
  modalClose() {
    this.NgbModalRef.close();
  }

  callNotification(){
    setInterval(function(){ NotificationComponent.temp.getNotificcation(0); }, 30000);
    // setInterval(this.getNotificcation(), 10000);
    // this.getNotificcation()
  }


  getNotificcation(offset){
    
    const rqstData = JSON.stringify({
      limit: 10,
      offset: offset
    });
    const result = this.callService(ApiUrls.getNotificationList, rqstData).subscribe((json: Object) => {
         this.notification = new Notification().fromJSON(json);
         if (this.notification.status === 'success') {
           this.notificationList = this.notification.data;
           this.notificationCount = this.notification.unread_count;
           this.notifCountFnOutput.emit(this.notificationCount);
        // //console.log(this.notificationList);
           this.loading = false;
           this.noNotification = false;
             return;
         }else {
           this.zone.run(() => {
           // this.router.navigateByUrl('login');
           this.loading = false;
           this.noNotification = true;
           });
         }

        }, error => {
          this.errorMessage = <any>error;
        });
  }

  readNotification(ntId, notifiType){
 // //console.log('calling read notification');
    const rqstData = JSON.stringify({
      ntId: ntId,
      notifType: notifiType
    });
    const result = this.callService(ApiUrls.readNotification, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
      // //console.log(this.statusClass);
         if (this.statusClass.status === 'success') {
             this.getNotificcation(0);
          // //console.log(this.statusClass);
             return;
         }else {
           this.zone.run(() => {
           // this.router.navigateByUrl('login');
           });
         }

        }, error => {
          this.errorMessage = <any>error;
        });
  }

  markAllRead(){
    const rqstData = '';
    const result = this.callService(ApiUrls.markAllRead, rqstData).subscribe((json: Object) => {
      this.statusClass = new StatusClass().fromJSON(json);
   // //console.log(this.statusClass);
      if (this.statusClass.status === 'success') {
        this.getNotificcation(0);
     // //console.log(this.statusClass);
        return;
      } else {
        this.zone.run(() => {
          // this.router.navigateByUrl('login');
        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });
  }


  /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

 // //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
 // //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }


}
