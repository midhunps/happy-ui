import { Component, OnInit, Renderer2 } from '@angular/core';
import {Global} from '../../../models/global';

@Component({
  selector: 'app-res-menu',
  templateUrl: './res-menu.component.html',
  styleUrls: ['./res-menu.component.scss']
})
export class ResMenuComponent implements OnInit {
  isOpen = false;
  constructor(private renderer: Renderer2, public global: Global) { }

  ngOnInit() {
  }
  resMenu() {
    this.isOpen = !this.isOpen;
    if (this.isOpen === true) {
      this.renderer.addClass(document.body, 'modal-open');
      this.renderer.addClass(document.body, 'menu-open');
    } else {
      this.renderer.removeClass(document.body, 'modal-open');
      this.renderer.removeClass(document.body, 'menu-open');
    }
  }

}
