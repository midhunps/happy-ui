import { Component, OnInit, Output, EventEmitter, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: ['./main-search.component.scss']
})
export class MainSearchComponent implements OnInit {
  
  @Output() searchFnOutput = new EventEmitter<any>();
  searchForm: FormGroup;
  userdata: any = {};
  constructor(fb: FormBuilder, private zone: NgZone) {
  	this.searchForm = fb.group({
	    'searchStr': [null, Validators.required]
	})
  }

  ngOnInit() {
  }

  searchWithStr(userdata){
  	this.searchFnOutput.emit(userdata.searchStr);
  }

}
