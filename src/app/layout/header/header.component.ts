import { Component, OnInit, Inject, NgZone, ElementRef, OnChanges, EventEmitter, Output } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { OwnDetails } from '../../models/own-details';
import { ApiUrls } from '../../models/api-urls';
import {Global} from '../../models/global';
import 'rxjs/Rx';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnChanges {

	profile_pic: string;
	constructor(private elRef: ElementRef,  private http:Http, private zone: NgZone, private router: Router, public global: Global) {
		this.profile_pic = global.profilePic;
	}

	errorMessageBox;
	loader;
	errorMessage;
	access_token: string = localStorage.getItem('access_token');
	ownDetails: OwnDetails;
	profileName: string = '';
	profileImage: string = '';
	mediaUrl: string = ApiUrls.mediaUrl;
	unreadNotifications;
	offsetTop;
	fixedHeader = false;
	fixedActive = false;
	position = 0;
	headerHeight = 76;
	@Output() postReload = new EventEmitter<string>();

	ngOnChanges() {
		this.headerHeight = this.elRef.nativeElement.querySelector('header').offsetHeight + 15;
	}
	ngOnInit() {
		this.getOwnProfile(this.access_token);
		window.addEventListener('scroll', this.scroll, true);
  }
  scroll = (): void => {
		this.offsetTop = document.documentElement.scrollTop;
		// //console.log('rth', this.offsetTop);
    if (this.offsetTop >= 100) {
			this.fixedHeader = true;
			this.headerHeight = this.elRef.nativeElement.querySelector('header').offsetHeight + 15;
			// downwards or upwards
      if (this.offsetTop > this.position) {
				// //console.log("scrolling downwards");
				this.fixedActive = false;
      } else {
				// //console.log("scrolling upwards");
				this.fixedActive = true;
      }
			this.position = this.offsetTop;
			// downwards or upwards end
    } else {
			this.fixedHeader = false;
			this.fixedActive = false;
		}
	}
	reloadPost() {
		this.postReload.emit('reload');
		window.scrollTo(0, 0);
	}
	
	searchWithstr(event){
		// console.log(event);
	    this.router.navigate(['search', event])
	}

	notificationCount(event){
		this.zone.run(() => {
			this.unreadNotifications = event;
		})
	}

	getOwnProfile(access_token){
		const rqstData = '';
		// console.log(access_token);
		const result = this.callService(ApiUrls.ownProfile, rqstData).subscribe((json: Object) => {
         this.ownDetails = new OwnDetails().fromJSON(json);
         if (this.ownDetails.status === 'success') {
         	this.printOwnDetails(this.ownDetails.data);
           	return;
         }else {
         	this.zone.run(() => {
		     	// this.router.navigateByUrl('login');
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}

	printOwnDetails(data){
		// console.log(data);
		this.zone.run(() =>{
			this.profileName = data.first_name+" "+data.last_name;
			this.profileImage = data.profile_photo;
			this.global.profilePic = this.profileImage;
			this.profile_pic = this.mediaUrl+this.profileImage;
			this.global.profilePic = this.profile_pic;
			this.global.profileName = this.profileName;
			this.global.homepage_post = data.homepage_post;
		});
	}


	/**
 	 * api calling
  	*/
	callService(url, userdata): Observable<Response[]> {
	  let headers = new Headers(
	  		{ 
	  			'Content-Type': 'application/json',
	  			'token': this.access_token 
	  		}
	  	);
	  let options = new RequestOptions({ headers: headers });

	  // console.log(userdata);
	  return this.http.post(url, userdata, options)
	    .map((res: Response) => res.json()
	    )
	    .catch(this.handleErrorObservable);
	}

	private handleErrorObservable(error: Response | any) {
	  // console.log("Error");
	  if (error.status == 401) {
	    // window.location.href="./#/login";
	  }
	  return Observable.throw(JSON.parse(error._body).errorMessage || error);
	}

	private setHeaders(): Headers {
	  const headersConfig = {
	    'Content-Type': 'application/json'
	  };
	  return new Headers(headersConfig);
	}


}
