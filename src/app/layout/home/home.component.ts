import { Component, OnInit, Inject, NgZone, HostListener } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm} from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { Login } from '../../models/login';
import { ApiUrls } from '../../models/api-urls';
import 'rxjs/Rx';

@HostListener("window:scroll", [])
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title = 'Happy Equities | Home';

  constructor(fb: FormBuilder, private http:Http, private zone: NgZone, private router: Router) {

    //console.log('scrolling');
    //console.log('height :' + window.innerHeight);
    //console.log('scrollY :' + window.scrollY);
    //console.log('document.body.offsetHeight : ' + document.body.offsetHeight);

    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            // you're at the bottom of the page
            //console.log('page at bottom');
            // alert();
        }
    
  }

  access_toke: string = localStorage.getItem('access_token');
  ngOnInit() {
  	this.manageUser(this.access_toke);
  }

  manageUser(token){
  	if(token == null){
  		// this.router.navigateByUrl('login');
  	}
  }

  onScroll(): void {
    //console.log('scrolling');
  if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          // you're at the bottom of the page
          //console.log('page at bottom');
          alert();
      }
  }


}
