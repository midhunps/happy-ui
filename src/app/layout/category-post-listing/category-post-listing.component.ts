import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-category-post-listing',
  templateUrl: './category-post-listing.component.html',
  styleUrls: ['./category-post-listing.component.scss']
})
export class CategoryPostListingComponent implements OnInit {

  constructor(
    private title: Title,
    private meta: Meta
    ) { }

  ngOnInit() {
    this.title.setTitle('Happy Equities - Have a safe investment through group discussions with experienced Investors / Traders in the world of Market related investments. Welcome to HappyEquities - Learn Together, Earn Together');
    this.meta.addTag({ name: 'description', content: 'description" content="Have a safe investment through group discussions with experienced Investors / Traders in the world of Market related investments. Welcome to HappyEquities - Learn Together, Earn Together' })
  }

}
