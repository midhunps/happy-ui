import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPostListingComponent } from './category-post-listing.component';

describe('CategoryPostListingComponent', () => {
  let component: CategoryPostListingComponent;
  let fixture: ComponentFixture<CategoryPostListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryPostListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPostListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
