import { Component, OnInit, Output, EventEmitter, Inject, NgZone, ViewChild, ViewChildren } from '@angular/core';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm} from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivateProfile } from '../../models/prelogin/activate-profile';
import { ApiUrls } from '../../models/api-urls';
import { SidebarClass } from '../../models/homepage/sidebar-class';
import { SidebarGroups } from '../../models/category/sidebar-groups';
import { StatusClass } from '../../models/status-class';
import { Global } from '../../models/global';
import { MenuItemComponent } from '../left-menus/menu-item/menu-item.component';
import 'rxjs/Rx';

@Component({
  selector: 'app-left-menus',
  templateUrl: './left-menus.component.html',
  styleUrls: ['./left-menus.component.scss']
})
export class LeftMenusComponent implements OnInit {
  @Output() output = new EventEmitter<string>();
  @Output() categoryChange = new EventEmitter<any>();
  @ViewChild(MenuItemComponent) child;
  access_token: string = localStorage.getItem('access_token');
  token;
  sidebarClass: SidebarClass;
  statusClass: StatusClass;
  sidebarGroups: SidebarGroups;
  activationMessage;
  errorMessage;
  inviteForm: FormGroup;
  userdata: any = {};
  sidebarCategories;
  sidebarGroupsList;
  isValid = true;
  isCollapsed = [];




  constructor(
    private route: ActivatedRoute,
      private http:Http, 
      private zone: NgZone, 
      private router: Router,  
      fb: FormBuilder,
     public global: Global) {
    this.inviteForm = fb.group({
          'email': [null, Validators.required]
      })
  }

  ngOnInit() {
    this.getSidebarCategories();
  }
  resMenu() {
    this.output.emit('complete');
  }
  test() {
    this.getSidebarCategories();
  }

  gotoCategory(term:string){
    this.child.gotoCategory(term);
    this.global.catIvalid = true;
  }
  groupClose(event) {
    const allGroupNames = [];
    this.sidebarGroupsList.forEach(element => {
      if (element.group_name != event) {
        allGroupNames.push(element.group_name);
      }
    });
    if (this.isCollapsed[event] == undefined) {
      this.isCollapsed[event] = true;
    } else {
      this.isCollapsed[event] = !this.isCollapsed[event];
    }
    allGroupNames.forEach(element => {
      this.isCollapsed[element] = false;
    });
  }
  getSidebarCategories(){
    this.global.catIvalid = true;
    this.isValid = true;
    //console.log('sidebar categories');
    const rqstData = '';
    const result = this.callService(ApiUrls.sidemenuCategories, rqstData).subscribe((json: Object) => {
         this.sidebarGroups = new SidebarGroups().fromJSON(json);
         if (this.sidebarGroups.status === 'success') {
           this.global.catIvalid = false;
           this.sidebarGroupsList = this.sidebarGroups.data;
           //console.log(this.sidebarCategories);
           this.isValid = false;
           return;
         } else {
           this.zone.run(() => {
             //console.log(this.sidebarGroups.message);
             this.global.catIvalid = false;
           });
         }

    }, error => {
      this.errorMessage = <any>error;
    });
  }

  inviteAFriend(userdata){
    //console.log(userdata);

    const rqstData = JSON.stringify({
      emailAddress: userdata.email
    });

    const result = this.callService(ApiUrls.inviteFriend, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         if (this.statusClass.status === 'success') {
           //console.log(this.statusClass);
           this.userdata.email = '';
           alert('Invitation sent')
           return;
         }else {
           this.zone.run(() => {
             //console.log(this.statusClass);
             this.userdata.email = '';
           });
         }

    }, error => {
      this.errorMessage = <any>error;
    });

  }

  goToCategory(id){
    this.child.gotoCategory(id);
  }

  /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
