import { Component, OnInit, Input, Output, EventEmitter, Inject, NgZone } from '@angular/core';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { ActivateProfile } from '../../../models/prelogin/activate-profile';
import { ApiUrls } from '../../../models/api-urls';
import { SidebarClass } from '../../../models/homepage/sidebar-class';
import { SidebarCategories } from '../../../models/category/sidebar-categories';
import { StatusClass } from '../../../models/status-class';
import { PostList } from '../../../models/post/post-list';
import { CategoryForm } from '../../../models/category/category-form';
import { CategoryInfo } from '../../../models/category/category-info';
import { Global } from '../../../models/global'
import 'rxjs/Rx';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {
  @Output() output = new EventEmitter<string>();
  @Output() groupClose = new EventEmitter<string>();
  @Input() groupId: any;
  @Input() groupName: any;

  sidebarCategories:SidebarCategories;
  access_token: string = localStorage.getItem('access_token');
  errorMessage;
  sidebarCategoriesList;
  categoryFormLength;

  postList: PostList;
  categoryForm: CategoryForm;
  categoryInfo: CategoryInfo;
  @Input() isCollapsed: any;

  constructor(
    private route: ActivatedRoute,
    private http:Http,
    private zone: NgZone,
    private router: Router,
    private global: Global
  ) {

  }

  ngOnInit() {
  	this.getSidebarCategories(this.groupId);
  }
  toggleChild(name) {
    this.groupClose.emit(name);
  }
  gotoCategory(term:string){
    // this.router.navigate(['category', {term: term}]);
    this.global.categoryId = term;
    this.getPostList(0, term);
    this.getCategoryForm(term);
    this.getCategoryInfo(term);
    this.router.navigate(['category', term]);
    this.global.catIvalid = true;
  }

  getSidebarCategories(groupId){
    
    const rqstData = JSON.stringify({
    	groupId: groupId
    });

    const result = this.callService(ApiUrls.sideMenuCategoriesWithGroup, rqstData).subscribe((json: Object) => {
         this.sidebarCategories = new SidebarCategories().fromJSON(json);
         if (this.sidebarCategories.status === 'success') {
           this.sidebarCategoriesList = this.sidebarCategories.data;
           //console.log(this.sidebarCategories);
           return;
         }else {
           this.zone.run(() => {
             //console.log(this.sidebarCategories.message);
           });
         }

    }, error => {
      this.errorMessage = <any>error;
    });
  }
  resMenu() {
    this.output.emit('complete');
  }

  getCategoryForm(categoryId) {
    const rqstData = JSON.stringify(
      {
        categoryId: categoryId
      });

    const result = this.callService(ApiUrls.getCategoryForm, rqstData).subscribe((json: Object) => {
      this.categoryForm = new CategoryForm().fromJSON(json);
      //console.log(this.categoryForm);
      if (this.categoryForm.status === 'success') {
        this.categoryFormLength = this.categoryFormLength + this.categoryForm.data.length;
        this.global.categoryForm = this.categoryForm.data;
        return;
      } else {
        this.zone.run(() => {
          this.global.categoryForm = [];
        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });
  }

  getCategoryInfo(categoryId) {
    const rqstData = JSON.stringify(
      {
        "categoryId": categoryId
      });

    const result = this.callService(ApiUrls.getCategoryInfo, rqstData).subscribe((json: Object) => {
      this.categoryInfo = new CategoryInfo().fromJSON(json);
      //console.log(this.categoryInfo);
      if (this.categoryInfo.status === 'success') {
        
        this.global.catIvalid = false;
        this.global.categoryInfo = this.categoryInfo.data;
        return;
      } else {
        this.zone.run(() => {

        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });

  }

  getPostList(offset, categoryId) {
    //console.log(categoryId);
    if (categoryId != 'followed') {
      
      const rqstData = JSON.stringify(
        {
          offset: offset,
          limit: 10,
          categoryId: categoryId
        }
      );

      //console.log(rqstData);
      const result = this.callService(ApiUrls.categoryBasedPosts, rqstData).subscribe((json: Object) => {
        this.postList = new PostList().fromJSON(json);
        //console.log(this.postList);
        
        if (this.postList.status === 'success') {

          this.global.categoryPostList = this.postList.data;
          this.global.categoryPostCheck = 'show';
          return;
        } else {
          this.zone.run(() => {
            this.global.categoryPostList = [];
            this.global.categoryPostCheck = '';
          });
        }

      }, error => {
        this.errorMessage = <any>error;
      });
    } else {
      
      const rqstData = JSON.stringify(
        {
          offset: offset,
          limit: 10,
        }
      );

      //console.log(rqstData);
      const result = this.callService(ApiUrls.followedPosts, rqstData).subscribe((json: Object) => {
        this.postList = new PostList().fromJSON(json);
        //console.log(this.postList);
        if (this.postList.status === 'success') {
          this.global.catIvalid = false;
          this.global.categoryPostList = this.postList.data;
          return;
        } else {
          this.global.categoryPostList = undefined;
          this.zone.run(() => {
          this.global.catIvalid = false;
          });
        }

      }, error => {
        this.errorMessage = <any>error;
      });
    }
  }


  /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
