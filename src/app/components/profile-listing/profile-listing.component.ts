import { Component, OnInit, Input, NgZone, ViewEncapsulation, ElementRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiUrls } from '../../models/api-urls';
@Component({
  selector: 'app-profile-listing',
  templateUrl: './profile-listing.component.html',
  styleUrls: ['./profile-listing.component.scss']
})
export class ProfileListingComponent implements OnInit {
  
  @Input() pro: any;
  mediaUrl: string = ApiUrls.mediaUrl;
  constructor(private modalService: NgbModal, private elRef: ElementRef, fb: FormBuilder, private http:Http, private zone: NgZone, private router: Router) { }

  ngOnInit() {
  }

  goToProfile(username){
    //console.log(username);
    this.router.navigate(['profileview', username])
  }

}
