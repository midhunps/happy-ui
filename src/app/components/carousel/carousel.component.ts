import {Component, OnInit, Inject, NgZone} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import {HttpClient} from '@angular/common/http';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OwnDetails } from '../../models/own-details';
import { Advertisement } from '../../models/advertisement';
import { ApiUrls } from '../../models/api-urls';
import {Global} from '../../models/global'
import 'rxjs/Rx';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  images: Array<string>;
  advertisement: Advertisement;
  errorMessageBox;
  loader;
  errorMessage;
  access_token: string = localStorage.getItem('access_token');
  advertisementList;
  mediaUrl: string = ApiUrls.mediaUrl;

  constructor(private _http: HttpClient, private http:Http, private zone: NgZone, private router: Router, private global: Global) {}

  ngOnInit() {
    /*this._http.get('https://picsum.photos/list')
        .pipe(map((images: Array<{id: number}>) => this._randomImageUrls(images)))
        .subscribe(images => this.images = images);*/
    this.getAdvertisement();
  }

  private _randomImageUrls(images: Array<{id: number}>): Array<string> {
    return [1, 2, 3].map(() => {
      const randomId = images[Math.floor(Math.random() * images.length)].id;
      return `https://picsum.photos/900/500?image=${randomId}`;
    });
  }


  getAdvertisement(){
    const rqstData = '';
    const result = this.callService(ApiUrls.getAdvertisements, rqstData).subscribe((json: Object) => {
         this.advertisement = new Advertisement().fromJSON(json);
         if (this.advertisement.status === 'success') {
             this.advertisementList = this.advertisement.data;
             //console.log(this.advertisementList);
             return;
         }else {
           this.zone.run(() => {
           // this.router.navigateByUrl('login');
           });
         }

        }, error => {
          this.errorMessage = <any>error;
        });
  }


  /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }
}

