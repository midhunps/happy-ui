import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { ProfileViewComponent } from './pages/profile-view/profile-view.component';
import { PostListingComponent } from './pages/post-listing/post-listing.component';
import { PostDetailedComponent } from './pages/post-listing/post-detailed/post-detailed.component';
import { DetailedPostComponent } from './pages/post-listing/post/detailed-post/detailed-post.component';
import { NotificationComponent } from './layout/header/notification/notification.component';
import { HomeComponent } from './layout/home/home.component';
import { CategoryPostListingComponent } from './layout/category-post-listing/category-post-listing.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { SignupComponent } from './pages/signup/signup.component';
import { SearchComponent } from './pages/search/search.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ActivateProfileComponent } from './pages/activate-profile/activate-profile.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CategoryComponent } from './pages/category/category.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent, children: [
      {
        path: '', component: CategoryPostListingComponent, children: [
          { path: '', component: PostListingComponent },
          { path: 'category/:term', component: CategoryComponent },
        ]
      },
      // { path: '', component: PostListingComponent },
      { path: 'user', component: UserProfileComponent },
      { path: 'notification', component: NotificationComponent },
      { path: 'profileview/:term', component: ProfileViewComponent },
      { path: 'search/:term', component: SearchComponent },
      { path: 'search', component: SearchComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'advertise-here', component: ContactComponent },
      { path: 'post/:postId', component: DetailedPostComponent },
    ]
  },
  {
    path: 'home', component: HomeComponent, children: [
      {
        path: '', component: CategoryPostListingComponent, children: [
          { path: '', component: PostListingComponent },
          { path: 'category/:term', component: CategoryComponent },
        ]
      },
      { path: 'user', component: UserProfileComponent },
      { path: 'notification', component: NotificationComponent },
      { path: 'profileview/:term', component: ProfileViewComponent },
      { path: 'search/:term', component: SearchComponent },
      { path: 'search', component: SearchComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'advertise-here', component: ContactComponent },
      { path: 'category/:term', component: CategoryComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'activate-profile', component: ActivateProfileComponent },
  { path: 'activate-profile/:token', component: ActivateProfileComponent },
  { path: 'forgot', component: ForgotPasswordComponent },
  { path: 'forgot/:token', component: ForgotPasswordComponent },
];

@NgModule({
  // imports: [ RouterModule.forRoot(routes)],
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }
