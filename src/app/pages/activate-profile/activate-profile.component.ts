import { Component, OnInit, Inject, NgZone } from '@angular/core';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm} from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivateProfile } from '../../models/prelogin/activate-profile';
import { ApiUrls } from '../../models/api-urls';
import { Global } from '../../models/global'
import 'rxjs/Rx';

@Component({
  selector: 'app-activate-profile',
  templateUrl: './activate-profile.component.html',
  styleUrls: ['./activate-profile.component.scss']
})
export class ActivateProfileComponent implements OnInit {

	token;
	activateProfileClass: ActivateProfile;
	activationMessage;
	errorMessage;
	activateForm: FormGroup;
	userdata: any = {};
	loader = false;
	

  constructor(
		private route: ActivatedRoute,  
		private http:Http, 
		private zone: NgZone, 
		private router: Router,  
		fb: FormBuilder, 
		public global: Global
		) {
			this.route.params.subscribe( params => {this.token = params.token} );
  	// this.route.params.subscribe( params => {this.token = params} );
  	this.activateForm = fb.group({
      	'token': [null, Validators.required]
    })
  }

  ngOnInit() {
		if(this.token != undefined){
			this.userdata = {
				token: this.token
			}
			this.activateProfile(this.userdata);
		}
  }

  activateProfile(userdata){
		this.loader = true;
  	//console.log(userdata);
  	const activateToken = userdata.token;
  	const activateUrl = ApiUrls.activateProfile+activateToken;
  	const result = this.callService(activateUrl).subscribe((json: Object) => {
         this.activateProfileClass = new ActivateProfile().fromJSON(json);
         if (this.activateProfileClass.status === 'success') {
         	//console.log(this.activateProfileClass);
					 this.activationMessage = this.activateProfileClass.message;
					 this.loader = false;
         	setTimeout((router: Router) => {
	            this.router.navigateByUrl('/login');
	          }, 3000);
           return;
         }else {
         	this.zone.run(() => {
						this.loader = false;
         		this.activationMessage = this.activateProfileClass.message;
		     	//console.log(this.activateProfileClass.message);
         	});
         }

    }, error => {
      this.errorMessage = <any>error;
    });
  }

  /**
 	 * api calling
  	*/
	callService(url): Observable<Response[]> {
	  let headers = new Headers({ 'Content-Type': 'application/json' });
	  let options = new RequestOptions({ headers: headers });

	  //console.log(url);
	  return this.http.get(url, options)
	    .map((res: Response) => res.json()
	    )
	    .catch(this.handleErrorObservable);
	}

	private handleErrorObservable(error: Response | any) {
	  //console.log("Error");
	  if (error.status == 401) {
	    window.location.href="./#/login";
	  }
	  return Observable.throw(JSON.parse(error._body).errorMessage || error);
	}

	private setHeaders(): Headers {
	  const headersConfig = {
	    'Content-Type': 'application/json'
	  };
	  return new Headers(headersConfig);
	}

}
