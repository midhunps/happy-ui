import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm, FormArray, FormControl } from '@angular/forms';
import { StatusClass } from '../../models/status-class';
import { ApiUrls } from '../../models/api-urls';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  static temp: ContactComponent;

  contactForm: FormGroup;
  errorMessageBox;
  loader;
  errorMessage;
  access_token: string = localStorage.getItem('access_token');
  userdata: any = {};
  statusClass: StatusClass;
  currentUrl;

  uploadFileBase64: string = '';
  uploadFileType: string = '';
  formLoader = false;

  constructor(
    private fb: FormBuilder,
    private http: Http,
    private router: Router,
  ) {
    ContactComponent.temp = this;
    this.contactForm = fb.group({
      name: [''],
      email: [''],
      phone: [''],
      mobile: [''],
      message: ['']
    });
  }

  ngOnInit() {
    this.getCurrentPage();
  }

  getCurrentPage() {
    const currentUrl = this.router.url;
    let lastIndex = currentUrl.lastIndexOf('/');
    lastIndex = lastIndex - 1;
    this.currentUrl = currentUrl.substr(1, lastIndex);
  }

  contact(userdata) {
    this.formLoader = true;
    let enquiryType = '';
    if (this.currentUrl === 'contact') {
      enquiryType = 'contact';
    } else {
      enquiryType = 'advertisement';
    }

    const rqstData = JSON.stringify(
      {
        name: userdata.name,
        email: userdata.email,
        phone: userdata.phone,
        mobile: userdata.mobile,
        message: userdata.message,
        enquiryType: enquiryType,
        fileData: this.uploadFileBase64,
        fileType: this.uploadFileType,
      }
    );
    //console.log(rqstData);

    const result = this.callService(ApiUrls.enquiry, rqstData).subscribe((json: Object) => {
      this.statusClass = new StatusClass().fromJSON(json);
      
      if (this.statusClass.status === 'success') {
        this.formLoader = false;
        alert(this.statusClass.message);
        this.contactForm.reset();
      } else {
        alert(this.statusClass.message);
        this.formLoader = false;
      }

    }, error => {
      this.errorMessage = <any>error;
      this.formLoader = false;
    });
  }


  uploadFileShowing(event) {
    const file_a = event.srcElement.files[0];
    ////console.log(file_a);
    const fileName_str = file_a.name;
    const fileType_str = file_a.type;
    let lastIndex = fileType_str.lastIndexOf('/');
    lastIndex++;
    const baseTpeLength = fileType_str.length;
    const fileType = fileType_str.substring(lastIndex, baseTpeLength);

    lastIndex--;
    const postType = fileType_str.substring(0, lastIndex);

    this.getBase64(file_a, this.saveFileArray, fileType, postType);
    
  }

  getBase64(file, callback, fileType, postType) {
    //console.log(fileType);
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      let base64Value = reader.result;
      let lastIndex = base64Value.lastIndexOf(',');
      lastIndex++;
      const base64Length = base64Value.length;
      const converted64 = base64Value.substring(lastIndex, base64Length);

      callback(converted64, fileType, postType);
    };
    reader.onerror = function (error) {
      //console.log('Error: ', error);
    };
  }

  saveFileArray(base64, fileType, postType) {
    ContactComponent.temp.uploadFileBase64 = base64;
    ContactComponent.temp.uploadFileType = fileType;
  }

	/**
	* api calling
	*/
  callService(url, userdata): Observable<Response[]> {
    const headers = new Headers(
      {
        'Content-Type': 'application/json',
        'token': this.access_token
      }
    );
    const options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href = "./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
