import { Component, OnInit, Inject, NgZone, EventEmitter, Output, ElementRef } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http'
import { ApiUrls } from '../../models/api-urls';
import { Search } from '../../models/search/search';
import {Global} from '../../models/global';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchKeyword;
  searchingStr;
  errorMessageBox;
  errorMessage;
  access_token: string = localStorage.getItem('access_token');
  searchResult: Search;
  walpostList;
  profileList;
  filterType: string = 'both';
  searchForm: FormGroup;

  isPostLoading = false;
  isPostListLoading = false;
  postLoadfrom = 10;
  postTemp = 0;
  postEnd = false;
  loadedOffset;

  bottomOffset = 0;
  offsetTop = 0;
  windowHeight = 0;

  post_count: number = 0;
  profile_count: number = 0;

  constructor(
    private modalService: NgbModal,  
    fb: FormBuilder, 
    private http:Http, 
    private zone: NgZone, 
    private router: Router,
    public global: Global,
    private route: ActivatedRoute, private elRef: ElementRef,) {
  		this.route.params.subscribe( params => {this.searchKeyword = params} );
  		this.searchForm = fb.group({
	      	'searchStr': [null, Validators.required]
	    })
  }

  ngOnInit() {
    if (!this.global.profileName) {
			this.router.navigateByUrl('login');
		}
    this.searchingStr = this.searchKeyword.term;
  	this.startSearch(0);
  	this.filterType = 'post';
  }

  scroll = (): void => {
    this.bottomOffset = this.elRef.nativeElement.querySelector('div.btm-scroll').offsetTop;
    this.offsetTop = document.documentElement.scrollTop;
    this.windowHeight = window.innerHeight;
    this.offsetTop = this.offsetTop + this.windowHeight + 250;
    // //console.log(this.bottomOffset);
    // //console.log(this.offsetTop);
    if (this.bottomOffset <= this.offsetTop) {
      this.isPostListLoading = true;
      //console.log(this.postTemp);
      if (this.postTemp === 0 && this.postEnd === false) {
        this.postTemp = this.postTemp + 10;
        this.startSearch(this.postLoadfrom);
        this.loadedOffset = this.postLoadfrom - 10;
        this.postLoadfrom = this.postLoadfrom + 10;
        //console.log(this.postTemp);
      }
    }
  }

  changeFilterType(type){
  	this.filterType = type;
  }

  searchWithstr(event){
    //console.log(event);
    this.searchingStr = event;
    this.walpostList = [];
    this.profileList = [];
    this.filterType = 'both';
    this.startSearch(0);
  }

  startSearch(offset){
    const rqstData = JSON.stringify(
      	{
			searchstr: this.searchingStr,
			proOffset: offset,
			proLimit: 10,
			postOffset: offset,
			postLimit: 10,
			searchFilter: this.filterType
		}
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.search, rqstData).subscribe((json: Object) => {
         this.searchResult = new Search ().fromJSON(json);
         //console.log(this.searchResult);
         if (this.searchResult.status === 'success') {
         	//console.log(this.searchResult);
           
           if(this.searchResult.post_search != undefined){
             this.zone.run(() => {
         	    this.walpostList = this.searchResult.post_search.post_data;
              this.post_count = this.searchResult.post_search.post_count
             })
           }else{
             this.post_count = 0;
           }

           if(this.searchResult.profile_search != undefined){
             this.zone.run(() => {
         	    this.profileList = this.searchResult.profile_search.profile_data;
               this.profile_count = this.searchResult.profile_search.profile_count;
             })
           }else{
             this.profile_count = 0;
           }
           	return;
         }else {
         	this.zone.run(() => {
		     	// this.router.navigateByUrl('login');
         	});
         }

    }, error => {
      this.errorMessage = <any>error;
    });
  }

   /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
