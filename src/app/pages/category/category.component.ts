import { Component, OnInit, Inject, NgZone, ElementRef } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm, FormArray, FormControl } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { PostList } from '../../models/post/post-list';
import { ApiUrls } from '../../models/api-urls';
import { OwnDetails } from '../../models/own-details';
import { CategoryForm } from '../../models/category/category-form';
import { CategoryInfo } from '../../models/category/category-info';
import { StatusClass } from '../../models/status-class';
import { Global } from '../../models/global';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
	errorMessageBox;
	loader;
	errorMessage;
	access_token: string = localStorage.getItem('access_token');
	walpostList;
	postList:PostList;
	categoryForm:CategoryForm;
	categoryInfo:CategoryInfo;
	statusClass:StatusClass;
	categoryId;
	formList;
	contactForm: FormGroup;
	userdata;
	categoryFormLength = 6;
	categoryInfos;
	mediaUrl: string = ApiUrls.mediaUrl;
	isPostLoading = false;
	isValid = false;
	
  isPostListLoading = false;
  postLoadfrom = 10;
  postTemp = 0;
  postEnd = false;
  loadedOffset;

  bottomOffset = 0;
  offsetTop = 0;
	windowHeight = 0;
	followedLoader = true;
	bottomDiv = null;
	currentTimezone: string = "";

	constructor(
		private modalService: NgbModal,
		private fb: FormBuilder,
		private http:Http,
		private zone: NgZone,
		private router: Router,
		private route: ActivatedRoute,
		public global: Global,
		private elRef: ElementRef
	) {
		// this.route.params.subscribe( params => {this.categoryId = params} );
    // //console.log(this.categoryId);
    // this.getPostList(0);
		this.contactForm = fb.group({
			name: [''],
			email: [''],
			phone: [''],
			mobile: [''],
			message: ['']
		})

	}

	initContact() {
		return this.fb.group({
			name: [''],
			email: [''],
			phone: [''],
			mobile: [''],
			message: [''],
			interest: this.fb.array([
				this.initInterest()
			])
		});
	}

	initInterest() {
		return this.fb.group({
			chekboxName: ['']
		});
	}

	ngOnInit() {
		// this.getPostList(0);
		//console.log(this.global.categoryInfo);
		//console.log(this.global.categoryPostList);
		//console.log(this.global.categoryForm);
		
		this.userdata = {chekboxName : ''};
		this.global.categoryInfo = { category_name: ''};
		
		if (this.global.timezone == undefined) {
			this.getTimezone();
		}
		if (!this.global.profileName) {
			this.router.navigateByUrl('login');
		}

		this.route.paramMap.subscribe((params: ParamMap) => {
			if (params['params'].term == 'followed') {
				this.categoryId = 'followed';
				this.global.categoryInfo = { category_name: 'followed'};
				this.global.categoryId = 'followed';
			} else {
				let id = parseInt(params.get('term'));
				this.categoryId = id;
				this.global.categoryInfo = { category_name: id};
				this.global.categoryId = this.categoryId;
			}
			
		});

		/* if((this.global.categoryForm == undefined) || (this.global.categoryForm == '')){
			this.getCategoryForm();
		}else{
			this.formList = this.global.categoryForm;
		}

		if((this.global.categoryInfo == undefined) || (this.global.categoryInfo == '')){
			this.getCategoryInfo();
		}else{
			this.categoryInfos = this.global.categoryInfo;
		}

		if((this.global.categoryPostList == undefined) || (this.global.categoryPostList = '')){
			this.getPostList(0);
		}else{
			this.walpostList = this.global.categoryPostList;
		} */
		this.getCategoryForm();
		this.getCategoryInfo();
		this.getPostList(0);
		window.addEventListener('scroll', this.scroll, true);
	}
	
	getTimezone() {
		let date = new Date();
		const string = date.toString();
		const lastIndex = string.lastIndexOf('+');
		const stringLength = string.length;
		let timezone = string.substring(lastIndex, stringLength);
		const timezoneLength = timezone.lastIndexOf(' (');
		timezone = timezone.substring(0, timezoneLength);
		const timezone_a = timezone.split('', 5);
		this.currentTimezone = timezone_a[0] + timezone_a[1] + timezone_a[2] + ":" + timezone_a[3] + timezone_a[4];
		this.global.timezone = this.currentTimezone;

		//console.clear();
		//console.log(this.currentTimezone);
	}

  scroll = (): void => {
			this.bottomDiv = this.elRef.nativeElement.querySelector('div.btm-scroll');
			if (this.bottomDiv != null) {
				this.bottomOffset = this.bottomDiv.offsetTop;
				this.offsetTop = document.documentElement.scrollTop;
				this.windowHeight = window.innerHeight;
				this.offsetTop = this.offsetTop + this.windowHeight + 250;
				// //console.log(this.bottomOffset);
				// //console.log(this.offsetTop);
				if (this.bottomOffset <= this.offsetTop) {
					this.isPostListLoading = true;
					// //console.log(this.postTemp);
					if (this.postTemp === 0 && this.postEnd === false) {
						this.postTemp = this.postTemp + 10;
						this.getPostList(this.postLoadfrom);
						this.loadedOffset = this.postLoadfrom - 10;
						this.postLoadfrom = this.postLoadfrom + 10;
						// //console.log(this.postTemp);
					}
				}
			}
  }

  getPostList(offset){
		this.global.catIvalid = true;
			// console.log('reached', this.categoryId);
      if(this.categoryId != 'followed'){
				this.isPostLoading = true;
  	    const rqstData = JSON.stringify(
  	      {
  	        offset: offset,
  	        limit: 10,
						categoryId: this.categoryId,
						timezone: this.global.timezone
  	      }
  	    );

  	    //console.log(rqstData);
  	    const result = this.callService(ApiUrls.categoryBasedPosts, rqstData).subscribe((json: Object) => {
  	         this.postList = new PostList().fromJSON(json);
						 //console.log(this.postList);
						 this.isPostLoading = false;
						 this.followedLoader = false;
  	         if (this.postList.status === 'success') {
							this.global.catIvalid = false;
							 this.global.categoryPostList = this.postList.data;
  	             return;
  	         }else {
  	           this.zone.run(() => {
								 this.postEnd = true;
								// this.isPostListLoading = true;
								this.global.catIvalid = false;
  	           });
  	         }

  	      }, error => {
						this.errorMessage = <any>error;
  	      });
      }else{
				this.isValid = true;
        const rqstData = JSON.stringify(
          {
            offset: offset,
						limit: 10,
          }
        );

        //console.log(rqstData);
        const result = this.callService(ApiUrls.followedPosts, rqstData).subscribe((json: Object) => {
             this.postList = new PostList().fromJSON(json);
						 //console.log(this.postList);
             if (this.postList.status === 'success') {
             
							 this.global.categoryPostList = this.postList.data;
							 this.isValid = false;
                 return;
             }else {
               this.zone.run(() => {
								 this.global.categoryPostList = undefined;
               });
             }

          }, error => {
						this.errorMessage = <any>error;
						// this.global.categoryPostList = undefined;
          });
      }
  }

  editSuccess(event){
    //console.log(event)
    const index = event.index;
    const title = event.postTitle;
    const postId = event.postId;

    this.walpostList[index].post = title;
  }

  getCategoryForm(){

  	const rqstData = JSON.stringify(
	  	{
	  		categoryId: this.categoryId
	  	});

  	// //console.log(rqstData);
		const result = this.callService(ApiUrls.getCategoryForm, rqstData).subscribe((json: Object) => {
					this.categoryForm = new CategoryForm().fromJSON(json);
					// console.log(this.categoryForm);
					if (this.categoryForm.status === 'success') {
						this.categoryFormLength = this.categoryFormLength + this.categoryForm.data.length;
						this.global.categoryForm = this.categoryForm.data;
						return;
					}else {
						this.zone.run(() => {
						
						});
					}

		}, error => {
		this.errorMessage = <any>error;
		});
	}

  likeFun(event){
    ////console.log(event);
    //this.walpostList[event].is_liked = 1;
    const likes = parseInt(event.like_count);
    const index = event.index;
    this.walpostList[index].like_count = '';
    this.walpostList[index].like_count = likes + 1;
    this.walpostList[index].is_liked = 1;
  }

  unlikeFun(event){
    //console.log(event);
    // this.walpostList[index].is_liked = 0;
    const likes = parseInt(event.like_count);
    const index = event.index;
    this.walpostList[index].like_count = likes - 1;
    this.walpostList[index].is_liked = 0;
  }

  
  followFn(event){
    const follows = parseInt(event.follow_count);
    const index = event.index;
    this.walpostList[index].follow_count = follows + 1;
    this.walpostList[index].is_followed = 1;
  }

  unfollowFn(event){
    const follows = parseInt(event.follow_count);
    const index = event.index;
    this.walpostList[index].follow_count = follows - 1;
    this.walpostList[index].is_followed = 0;
  }

  
  submitForm(userdata){
  	let interest = [];
  	let formArray = document.getElementById("contactForm");
  	for(let i=0; i<this.categoryFormLength; i++){
  		if((formArray[i].type == 'checkbox') && (formArray[i].checked == true)){
  			interest.push(formArray[i].value);
  		}
  	}
  	
  	const rqstData = JSON.stringify(
  		{
			categoryId: this.categoryId,
			name: userdata.name,
			email: userdata.email,
			phone: userdata.phone,
			mobile: userdata.mobile,
			message: userdata.message,
			extraFields: interest
		}
  	);

  	//console.log(rqstData);

  	const result = this.callService(ApiUrls.categoryFormMail, rqstData).subscribe((json: Object) => {
         this.statusClass = new CategoryForm().fromJSON(json);

					if (this.statusClass.status === 'success') {
						alert(this.statusClass.message);
						let mainForm = this.elRef.nativeElement.querySelector('form');
						mainForm.reset();
					} else {
						alert(this.statusClass.message);
					}
         /*if (this.statusClass.status === 'success') {
			alert(this.statusClass.message);
             return;
         }else {
           this.zone.run(() => {
           
           });
         }*/

	}, error => {
	this.errorMessage = <any>error;
	});


  }

  getCategoryInfo(){
  	const rqstData = JSON.stringify(
		{
			"categoryId": this.categoryId
		});

  	const result = this.callService(ApiUrls.getCategoryInfo, rqstData).subscribe((json: Object) => {
         this.categoryInfo = new CategoryInfo().fromJSON(json);
         //console.log(this.categoryInfo);
         if (this.categoryInfo.status === 'success') {
					 this.global.categoryInfo = this.categoryInfo.data;
             return;
         }else {
           this.zone.run(() => {
           
           });
         }

		}, error => {
		this.errorMessage = <any>error;
		});

  }



	/**
	* api calling
	*/
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
