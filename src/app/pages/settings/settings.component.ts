import { Component, OnInit, Inject, NgZone } from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm} from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { Login } from '../../models/login';
import { ApiUrls } from '../../models/api-urls';
import { StatusClass } from '../../models/status-class';
import { OwnDetails } from '../../models/own-details';
import {Global} from '../../models/global';
import 'rxjs/Rx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  
	static temp: SettingsComponent;
	changePasswordForm: FormGroup;
	profileEditForm: FormGroup;
	ownDetails: OwnDetails;
	password_a: any = {};
	profileEdit_a: any = {};
	errorMessageBox;
	undefinedError;
	access_token: string = localStorage.getItem('access_token');
	statusClass:StatusClass;
	errorMessage

	uploadFileBase64;
	uploadFileType;

	profileName: string = '';
	profileImage: string = '';
	dateOfBirth: string = '';
	profile;
	mediaUrl: string = ApiUrls.mediaUrl;
	tempBase64Length;

	base64Value;

	constructor(fb: FormBuilder, private http:Http, private zone: NgZone, private router: Router, public global: Global) {
		SettingsComponent.temp = this;
		this.changePasswordForm = fb.group({
	  	'password': [null, Validators.required],
	   	'newPassword': [null, Validators.required],
	   	'confirmPassword': [null, Validators.required]
		});

		this.profileEditForm = fb.group({
		  	'firstName': [null, Validators.required],
		  	'lastName': [null, Validators.required],
		  	'dob': [null, Validators.required],
		  	'gender': [null, Validators.required],
		});
	}

	ngOnInit() {
		this.getOwnProfile();
	}

	getOwnProfile(){
		const rqstData = '';
		const result = this.callService(ApiUrls.ownProfile, rqstData).subscribe((json: Object) => {
         this.ownDetails = new OwnDetails().fromJSON(json);
         if (this.ownDetails.status === 'success') {
         	this.printOwnDetails(this.ownDetails.data);
           	return;
         }else {
         	this.zone.run(() => {
		     	this.router.navigateByUrl('login');
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}

	printOwnDetails(data){
		//console.log(data);
		this.zone.run(() =>{
			this.profile = data;
			this.profileName = data.first_name+" "+data.last_name;
			this.profileImage = data.profile_photo;

			this.profileEdit_a = {
				firstName: data.first_name,
				lastName: data.last_name,
				dob: data.dob,
				gender: data.gender,
			}

			this.dateOfBirth = data.dob;
		});
	}

	changePassword(password_a){

		this.errorMessageBox = '';
		this.undefinedError = '';
		const password = document.getElementById('psasword-container');
		const newpassword = document.getElementById('newpsasword-container');
		const confrimPassword = document.getElementById('confirmpsasword-container');



		if(password_a.password == undefined){
	    password.classList.add("error");
	    this.undefinedError = 'password error';
		}else{
	    password.classList.remove("error");
	    this.undefinedError = '';
		}

		if(password_a.newPassword == undefined){
	    newpassword.classList.add("error");
	    this.undefinedError = 'new passwor error';
		}else{
	    newpassword.classList.remove("error");
	    this.undefinedError = '';
		}

		if(password_a.confirmPassword == undefined){
	    confrimPassword.classList.add("error");
	    this.undefinedError = 'confirm password error';
		}else{
	    confrimPassword.classList.remove("error");
	    this.undefinedError = '';
		}

		if(this.undefinedError != ''){
			return false;
		}

		if(password_a.newPassword != password_a.confirmPassword){
	    newpassword.classList.add("error");
	    confrimPassword.classList.add("error");
	    this.errorMessageBox = 'password mismatch';
		}else{
	    newpassword.classList.remove("error");
	    confrimPassword.classList.remove("error");
	    this.errorMessageBox = '';
		}

		if(this.errorMessageBox != ''){
			return false;
		}


		const rqstData = JSON.stringify(
		{
			curr_password: password_a.password,
			new_password: password_a.newPassword
		});

		const result = this.callService(ApiUrls.changePassword, rqstData).subscribe((json: Object) => {
	     this.statusClass = new StatusClass().fromJSON(json);
	     //console.log(this.statusClass);
	     if(this.statusClass.status == 'success'){
	     	alert(this.statusClass.message);
	     	password.classList.remove("error");
	        newpassword.classList.remove("error");
	        confrimPassword.classList.remove("error");
	     }else{
	        password.classList.add("error");
	        newpassword.classList.add("error");
	        confrimPassword.classList.add("error");
	        alert(this.statusClass.message);
	     }

		}, error => {
		this.errorMessage = <any>error;
		});

	}

	profileUpdate(profileEdit_a){
		let firstName = '';
		let lastName = '';
		let gender = '';


		if((profileEdit_a.firstName) && (profileEdit_a.firstName !=undefined) && (profileEdit_a.firstName != null)){
			firstName = profileEdit_a.firstName;
		}
		if((profileEdit_a.lastName) && (profileEdit_a.lastName !=undefined) && (profileEdit_a.lastName != null)){
			lastName = profileEdit_a.lastName;
		}
		
		if((profileEdit_a.gender) && (profileEdit_a.gender !=undefined) && (profileEdit_a.gender != null)){
			gender = profileEdit_a.gender;
		}

		const dateOfBirth = profileEdit_a.dob.year+'-'+profileEdit_a.dob.month+'-'+profileEdit_a.dob.day;
		const rqstData = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			gender: gender
		});

		const result = this.callService(ApiUrls.editProfile, rqstData).subscribe((json: Object) => {
	     this.statusClass = new StatusClass().fromJSON(json);
	     //console.log(this.statusClass);
	     //console.log(this.tempBase64Length);
	     if(this.statusClass.status == 'success'){
	     	this.profileName = firstName+" "+lastName;
	     	this.global.profileName = this.profileName;
	     	alert(this.statusClass.message);
	     }else{
	        alert(this.statusClass.message);
	     }

		}, error => {
		this.errorMessage = <any>error;
		});
	}

	uploadFileShowing(event) {
    	const file_a = event.srcElement.files[0];
    	////console.log(file_a);
    	const fileName_str = file_a.name;
    	const fileType_str = file_a.type;
    	let lastIndex = fileType_str.lastIndexOf('/');
    	lastIndex++;
    	const baseTpeLength = fileType_str.length;
    	const fileType = fileType_str.substring(lastIndex, baseTpeLength);
    	
    	this.getBase64(file_a, this.saveFileArray, fileType);
  	}

  	getBase64(file, callback, fileType) {
	    //console.log(fileType);
	    var reader = new FileReader();
	    reader.readAsDataURL(file);
	    reader.onload = function () {
	      let base64Value = reader.result;
	      SettingsComponent.temp.base64Value = base64Value;
	      let lastIndex = base64Value.lastIndexOf(',');
	      lastIndex++;
		  const base64Length = base64Value.length;
	      const converted64 = base64Value.substring(lastIndex, base64Length);

	      callback(converted64, fileType);
	    };
	    reader.onerror = function (error) {
	      //console.log('Error: ', error);
	    };
	}

	saveFileArray(base64, fileType){
		SettingsComponent.temp.uploadFileBase64 = base64;
		SettingsComponent.temp.uploadFileType = fileType;
	}


	editProfilePic() {
		let photo = '';
		let fileType = '';

		if (this.uploadFileBase64 != undefined) {
			photo = this.uploadFileBase64;
			fileType = this.uploadFileType;
		}else{
			return false;
		}

		const rqstData = JSON.stringify({
			photo: photo,
			fileType: fileType
		});

		const result = this.callService(ApiUrls.editProfile, rqstData).subscribe((json: Object) => {
			this.statusClass = new StatusClass().fromJSON(json);
			//console.log(this.statusClass);
			//console.log(this.tempBase64Length);
			if (this.statusClass.status == 'success') {
				this.global.profilePic = this.base64Value;
				alert(this.statusClass.message);
			} else {
				alert(this.statusClass.message);
			}

		}, error => {
			this.errorMessage = <any>error;
		});
	}

	updateDateof(profileEdit_a){
		const dateOfBirth = profileEdit_a.dob.year + '-' + profileEdit_a.dob.month + '-' + profileEdit_a.dob.day;
		const rqstData = JSON.stringify({
			dob: dateOfBirth,
		});

		const result = this.callService(ApiUrls.editProfile, rqstData).subscribe((json: Object) => {
			this.statusClass = new StatusClass().fromJSON(json);
			//console.log(this.statusClass);
			//console.log(this.tempBase64Length);
			if (this.statusClass.status == 'success') {
				alert(this.statusClass.message);
				this.dateOfBirth = dateOfBirth;
			} else {
				alert(this.statusClass.message);
			}

		}, error => {
			this.errorMessage = <any>error;
		});
	}



	/**
 	 * api calling
  	*/
	callService(url, userdata): Observable<Response[]> {
	  let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
	  let options = new RequestOptions({ headers: headers });

	  //console.log(userdata);
	  this.tempBase64Length = userdata;
	  return this.http.post(url, userdata, options)
	    .map((res: Response) => res.json()
	    )
	    .catch(this.handleErrorObservable);
	}

	private handleErrorObservable(error: Response | any) {
	  //console.log("Error");
	  if (error.status == 401) {
	    window.location.href="./#/login";
	  }
	  return Observable.throw(JSON.parse(error._body).errorMessage || error);
	}

	private setHeaders(): Headers {
	  const headersConfig = {
	    'Content-Type': 'application/json'
	  };
	  return new Headers(headersConfig);
	}


}
