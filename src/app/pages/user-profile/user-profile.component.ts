import { Component, OnInit, Inject, NgZone, EventEmitter, Output, ElementRef } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { PostList } from '../../models/post/post-list';
import { ApiUrls } from '../../models/api-urls';
import { OwnDetails } from '../../models/own-details';
import {Global} from '../../models/global';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  
  	errorMessageBox;
	loader;
	errorMessage;
	access_token: string = localStorage.getItem('access_token');
	walpostList;
	postList:PostList;
	@Output() clsModal = new EventEmitter<string>();
	username;

	ownDetails: OwnDetails;
	profileName: string = '';
	profileImage: string = '';
	profile;
  isPostLoading = false;
  isPostListLoading = false;
  postLoadfrom = 10;
  postTemp = 0;
  postEnd = false;
  loadedOffset;
  isValid = true;
  bottomOffset = 0;
  offsetTop = 0;
  windowHeight = 0;
  currentTimezone: string = "";

  constructor(
    private modalService: NgbModal,
    fb: FormBuilder,
    private http:Http,
    private zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    public global: Global,
    private elRef: ElementRef
  ) {

  }

  ngOnInit() {
    if (this.global.timezone == undefined) {
      this.getTimezone();
    }
  	this.getPostList(0);
  	this.getOwnProfile(this.access_token);
  	this.profile = {email: ''};
    window.addEventListener('scroll', this.scroll, true);
  }

  getTimezone() {
    let date = new Date();
    const string = date.toString();
    const lastIndex = string.lastIndexOf('+');
    const stringLength = string.length;
    let timezone = string.substring(lastIndex, stringLength);
    const timezoneLength = timezone.lastIndexOf(' (');
    timezone = timezone.substring(0, timezoneLength);
    const timezone_a = timezone.split('', 5);
    this.currentTimezone = timezone_a[0] + timezone_a[1] + timezone_a[2] + ":" + timezone_a[3] + timezone_a[4];
    this.global.timezone = this.currentTimezone;

    //console.clear();
    //console.log(this.currentTimezone);
  }

  scroll = (): void => {
    this.bottomOffset = this.elRef.nativeElement.querySelector('div.btm-scroll').offsetTop;
    this.offsetTop = document.documentElement.scrollTop;
    this.windowHeight = window.innerHeight;
    this.offsetTop = this.offsetTop + this.windowHeight + 250;
    // //console.log(this.bottomOffset);
    // //console.log(this.offsetTop);
    if (this.bottomOffset <= this.offsetTop) {
      this.isPostListLoading = true;
      //console.log(this.postTemp);
      if (this.postTemp === 0 && this.postEnd === false) {
        this.postTemp = this.postTemp + 10;
        this.getPostList(this.postLoadfrom);
        this.loadedOffset = this.postLoadfrom - 10;
        this.postLoadfrom = this.postLoadfrom + 10;
        //console.log(this.postTemp);
      }
    }
  }

  getPostList(offset){
    this.isPostLoading = true;
    const rqstData = JSON.stringify(
      {
        offset: offset,
        limit: 10,
        username: '',
        timezone: this.global.timezone
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.ownPosts, rqstData).subscribe((json: Object) => {
         this.postList = new PostList().fromJSON(json);
         //console.log(this.postList);
         this.isPostLoading = true;
         if (this.postList.status === 'success') {
           this.walpostList = this.postList.data;

           this.isValid = false;
           this.isPostListLoading = false;
           this.postTemp = 0;
             return;
         }else {
           this.zone.run(() => {
            this.isPostListLoading = false;
            this.postTemp = 0;
            this.postEnd = true;
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });
  }

  likeFun(event){
    ////console.log(event);
    //this.walpostList[event].is_liked = 1;
    const likes = parseInt(event.like_count);
    const index = event.index;
    this.walpostList[index].like_count = '';
    this.walpostList[index].like_count = likes + 1;
    this.walpostList[index].is_liked = 1;
  }

  unlikeFun(event){
    //console.log(event);
    // this.walpostList[index].is_liked = 0;
    const likes = parseInt(event.like_count);
    const index = event.index;
    this.walpostList[index].like_count = likes - 1;
    this.walpostList[index].is_liked = 0;
  }

  
  followFn(event){
    const follows = parseInt(event.follow_count);
    const index = event.index;
    this.walpostList[index].follow_count = follows + 1;
    this.walpostList[index].is_followed = 1;
  }

  unfollowFn(event){
    const follows = parseInt(event.follow_count);
    const index = event.index;
    this.walpostList[index].follow_count = follows - 1;
    this.walpostList[index].is_followed = 0;
  }

  getOwnProfile(access_token){
		const rqstData = '';
		// //console.log(access_token);
		const result = this.callService(ApiUrls.ownProfile, rqstData).subscribe((json: Object) => {
         this.ownDetails = new OwnDetails().fromJSON(json);
         if (this.ownDetails.status === 'success') {
         	this.printOwnDetails(this.ownDetails.data);
           	return;
         }else {
         	this.zone.run(() => {
		     	this.router.navigateByUrl('login');
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}

	printOwnDetails(data){
		//console.log(data);
		this.zone.run(() =>{
			this.profile = data;
			this.profileName = data.first_name+" "+data.last_name;
			this.profileImage = data.profile_photo;
		});
	}


   /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
