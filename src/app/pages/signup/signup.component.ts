import { Component, OnInit, ViewEncapsulation, Inject, NgZone, ElementRef } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm} from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { SignupSuccess } from '../../models/prelogin/signup-success';
import { ApiUrls } from '../../models/api-urls';
import { Global } from '../../models/global';
import 'rxjs/Rx';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  now = new Date();

  static temp: SignupComponent;
  loadingAnimation = false;
  signupForm: FormGroup;
  userdata: any = {};
  signupSuccess: SignupSuccess;
  errorMessageBox;
  loader;

  errorMessage;
  emailError;
  firstNameError;
  lastNameError;
  dobError;
  passwordError;
  contactError;
  genderError;
  usernameError;
  signupMessage;
  uploadFilename = 'Choose Profile Picture';
  divUsername;
  stdError;
  agreeError;

  uploadFileBase64;
  uploadFileType;
  fileError = '';
  agreeElement;

  emailCond = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

  constructor(private modalService: NgbModal, fb: FormBuilder, private http: Http, private zone: NgZone, private router: Router, private elRef: ElementRef, private global: Global) {
    SignupComponent.temp = this;
    this.signupForm = fb.group({
          'username': [null, Validators.required],
          'password': [null, Validators.required],
          'first_name': [null, Validators.required],
          'last_name': [null, Validators.required],
          'email': [null, Validators.required],
          'contact': [null, Validators.required],
          'dob': [null, Validators.required],
          'gender': [null, Validators.required],
          'terms': [null, Validators.required],
          'std': [null, Validators.required],
          'day': [null, Validators.required],
          'month': [null, Validators.required],
          'year': [null, Validators.required],
      })
  }

  ngOnInit() {
    setTimeout(() => {
      this.elRef.nativeElement.querySelector('button.hidenReset').click();
    }, 100);
    this.agreeElement = this.elRef.nativeElement.querySelector('#checkbox1');
  }
  // modals
  openSm(terms) {
    this.modalService.open(terms, { size: 'sm' });
  }
  test() {
    //console.log(this.elRef.nativeElement.querySelector('option'));
    this.elRef.nativeElement.querySelector('option').click();
  }
  
  // model: NgbDateStruct = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};


  emailValidation(event){
      let newEmail = event.target.value;
      

      ////console.log(newEmail);

      if(newEmail.length>0){
        if (!newEmail.match(this.emailCond)) {
          //console.log('email not valid');
          this.zone.run(() => {
            // this.emailData = 'Please enter a valid email address';
            this.emailError = 'Please enter a valid email address';
            const email = document.getElementById('email-container');
            email.classList.add("error");
          });
        }else{
          this.zone.run(() => {
            this.emailError = "";
            const email = document.getElementById('email-container');
            email.classList.remove("error");
          });
        }
      }else{
        this.zone.run(() => {
          this.emailError = "";
          const email = document.getElementById('email-container');
          email.classList.remove("error");
        });
      }
      
      if(newEmail.length<=0){
        this.zone.run(() => {
          this.errorMessageBox = "";
          this.emailError = '';
          const email = document.getElementById('email-container');
          email.classList.remove("error");
        });
      }
  }

  onsignup(userdata){
    //console.log(userdata);
    this.errorMessage = '';
    this.formValidation(userdata);

    let profile_pic = '';
    let fileType = '';

    if(this.uploadFileBase64 != undefined){
      profile_pic = this.uploadFileBase64;
    }

    if(this.uploadFileType != undefined){
      fileType = this.uploadFileType;
    }
    const dob = userdata.year+'-'+userdata.month+'-'+userdata.day;

    this.loader = 'show';
    const rqstData = JSON.stringify(
      {
        username: userdata.username,
        password: userdata.password,
        first_name: userdata.first_name,
        last_name: userdata.last_name,
        dob: dob,
        email: userdata.email,
        contact: userdata.contact,
        gender: userdata.gender,
        document_type: fileType,
        profile_photo: profile_pic,
        std: userdata.std
      }
    );
    if (this.agreeElement.checked == true && userdata.year != null && userdata.month != null && userdata.day != null) {
      const result = this.callService(ApiUrls.signup, rqstData).subscribe((json: Object) => {
        this.signupSuccess = new SignupSuccess().fromJSON(json);
        if (this.signupSuccess.status === 'Success') {
          //console.log(this.signupSuccess);
          this.signupMessage = this.signupSuccess.message;
          this.global.activatingEmail = userdata.email;
          setTimeout((router: Router) => {
             this.router.navigateByUrl('/activate-profile');
           }, 2000);
          return;
        } else {
          this.zone.run(() => {
          // console.log(this.signupSuccess.message);
          this.errorMessage = this.signupSuccess.message;
           this.loader = '';
          });
        }
 
       }, error => {
         this.errorMessage = <any>error;
       });
    } else {
      this.loader = '';
    }
  }


  formValidation(userdata){
    if((userdata.email == '') || (userdata.email == undefined)){
      this.emailError = 'Please enter a valid email address';
      const element = document.getElementById('email-container');
      element.classList.add("error");
      this.errorMessage = 'error';
    } else {
      this.emailError = '';
      const element = document.getElementById('email-container');
      element.classList.remove("error");
    }
    if((userdata.username == '') || (userdata.username == undefined)){
      this.usernameError = 'Please enter a username';
      const element = document.getElementById('username-container');
      element.classList.add("error");
      this.errorMessage = 'error';
    } else {
      this.usernameError = '';
      const element = document.getElementById('username-container');
      element.classList.remove("error");
    }

    if((userdata.first_name == '') || (userdata.first_name == undefined)){
      this.firstNameError = 'Please enter your first name';
      const element = document.getElementById('firstname-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.firstNameError = '';
      const element = document.getElementById('firstname-container');
      element.classList.remove('error');
    }

    if((userdata.last_name == '') || (userdata.last_name == undefined)){
      this.lastNameError = 'Please enter your first name';
      const element = document.getElementById('lastname-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.lastNameError = '';
      const element = document.getElementById('lastname-container');
      element.classList.remove('error');
    }

    if((userdata.password == '') || (userdata.password == undefined)){
      this.lastNameError = 'Please enter your last name';
      const element = document.getElementById('password-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.lastNameError = '';
      const element = document.getElementById('password-container');
      element.classList.remove('error');
    }

    if((userdata.day == '') || (userdata.day == undefined) || (userdata.month == '') || (userdata.month == undefined) || (userdata.year == '') || (userdata.year == undefined)){
      this.dobError = 'Please select date of birth';
      const element = document.getElementById('dob-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.dobError = '';
      const element = document.getElementById('dob-container');
      element.classList.remove('error');
    }

    if((userdata.gender == '') || (userdata.gender == undefined)){
      this.genderError = 'Please select gender';
      const element = document.getElementById('gender-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.genderError = '';
      const element = document.getElementById('gender-container');
      element.classList.remove('error');
    }

    if((userdata.contact == '') || (userdata.contact == undefined)){
      this.contactError = 'Please enter contact number';
      const element = document.getElementById('contact-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.contactError = '';
      const element = document.getElementById('contact-container');
      element.classList.remove('error');
    }

    if((userdata.std == '') || (userdata.std == undefined)){
      this.stdError = 'Please enter isd code';
      const element = document.getElementById('std-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.stdError = '';
      const element = document.getElementById('std-container');
      element.classList.remove('error');
    }

    
    if((this.agreeElement.checked == false)){
      this.agreeError = 'Please agree Terms and Conditions';
      const element = document.getElementById('agree-container');
      element.classList.add('error');
      this.errorMessage = 'error';
    } else {
      this.agreeError = '';
      const element = document.getElementById('agree-container');
      element.classList.remove('error');
    }

    if (!userdata.email.match(this.emailCond)) {
      this.emailError = 'Please enter a valid email address';
      const email = document.getElementById('email-container');
      email.classList.add("error");
      this.errorMessage = 'error';
    } else {
      this.emailError = '';
      const email = document.getElementById('email-container');
      email.classList.remove("error");
    }

  }

  uploadFileShowing(event) {
      const file_a = event.srcElement.files[0];
      ////console.log(file_a);
      const fileName_str = file_a.name;
      const fileType_str = file_a.type;
      let lastIndex = fileType_str.lastIndexOf('/');
      lastIndex++;
      const baseTpeLength = fileType_str.length;
      const fileType = fileType_str.substring(lastIndex, baseTpeLength);
      
      this.getBase64(file_a, this.saveFileArray, fileType);
      this.uploadFilename = this.elRef.nativeElement.querySelector('input#upload-file').value;
      this.uploadFilename = this.uploadFilename.replace(/.*[\/\\]/, '');
    }

    getBase64(file, callback, fileType) {
      //console.log(fileType);
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        let base64Value = reader.result;
        let lastIndex = base64Value.lastIndexOf(',');
        lastIndex++;
        const base64Length = base64Value.length;
        const converted64 = base64Value.substring(lastIndex, base64Length);
      //console.log(base64Value);
      //console.log(base64Length);
      //console.log(lastIndex);
      //console.log(converted64);

        callback(converted64, fileType);
      };
      reader.onerror = function (error) {
        //console.log('Error: ', error);
      };
  }

  saveFileArray(base64, fileType){
    SignupComponent.temp.uploadFileBase64 = base64;
    SignupComponent.temp.uploadFileType = fileType;
  }



  /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}



