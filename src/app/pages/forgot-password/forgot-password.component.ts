import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm} from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { StatusClass } from '../../models/status-class';
import { ApiUrls } from '../../models/api-urls';
import 'rxjs/Rx';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

	loadingAnimation = false;
	token;
	forgotPassword: FormGroup;
	resetPass: FormGroup;
	userdata: any = {};
	resetData: any = {};
	statusClass: StatusClass;
	errorMessageBox;
	loader;
	errorMessage;
	emailError;
	passwordError;
	showForm: string = 'forgotPassword';
	hideToken = true;

	constructor( 
		fb: FormBuilder,
		private http:Http, 
		private zone: NgZone, 
		private router: Router,
		private route: ActivatedRoute,
	) {
		this.route.params.subscribe( params => {this.token = params.token} );
		this.forgotPassword = fb.group({
			'email': [null, Validators.required]
		})
		this.resetPass = fb.group({
			'token': [null, Validators.required],
			'password': [null, Validators.required],
			'confirmPassword': [null, Validators.required]
		})
}

	ngOnInit() {
		console.log(this.token);
		if(this.token != undefined){
			this.showForm = 'resetPass';
			this.hideToken = false;
			this.resetData = {token: this.token}
		}
	}

	forgotPass(userdata){
		//console.log(userdata);
		this.loader = true;
		this.emailError = '';
		this.errorMessage = '';

		if((userdata.email == '') || (userdata.email == undefined)){
			this.loader = false;
			const email = document.getElementById('email-container');
	        email.classList.add("error");
	        this.emailError = 'Please enter a email';
	        this.errorMessage = 'have error';
		}

		if(this.errorMessage != ''){
			return false;
		}

		const rqstData = JSON.stringify({
			email: userdata.email
		});
		const result = this.callService(ApiUrls.forgotPassword, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         if (this.statusClass.status === 'success') {
         	//console.log(this.statusClass);
           	//localStorage.setItem('access_token', this.login.access_toke);
			   this.showForm = 'resetPass'
			   this.loader = false;
            
           return;
         }else {
         	this.zone.run(() => {
		     	//console.log(this.statusClass.message);
		        this.loader = "";
		        
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });

	}

	resetPassword(resetData){
		//console.log(resetData)
		this.loader = true;

		this.errorMessage = '';
		if((resetData.token == '') || (resetData.token == undefined)){
			this.loader = '';
			const token = document.getElementById('token-container');
	        token.classList.add("error");
	        this.passwordError = 'Please enter a token';
	        this.errorMessage = 'have error';
		}
		if((resetData.password == '') || (resetData.password == undefined)){
			this.loader = '';
			const password = document.getElementById('password-container');
	        password.classList.add("error");
	        this.passwordError = 'Please enter a password';
	        this.errorMessage = 'have error';
		}
		if((resetData.confirmPassword == '') || (resetData.confirmPassword == undefined)){
			this.loader = '';
			const confirm = document.getElementById('confirm-password-container');
	        confirm.classList.add("error");
	        this.passwordError = 'Please enter a confirm password';
	        this.errorMessage = 'have error';
		}

		if(resetData.password != resetData.confirmPassword){
			this.loader = '';
			const confirm = document.getElementById('confirm-password-container');
	        const password = document.getElementById('password-container');
	        confirm.classList.add("error");
	        password.classList.add("error");
	        this.passwordError = 'Password mismatched';
	        this.errorMessage = 'have error';
		}

		if(this.errorMessage != ''){
			return false;
		}
		debugger
		const rqstData = JSON.stringify({
			token: resetData.token,
			password: resetData.password
		})
		const result = this.callService(ApiUrls.resetPassword, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         if (this.statusClass.status === 'success') {
         	//console.log(this.statusClass);
           	
            this.router.navigateByUrl('/login');
           return;
         }else {
         	this.zone.run(() => {
		     	//console.log(this.statusClass.message);
		        this.loader = "";
		        
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}



	/**
 	 * api calling
  	*/
	callService(url, userdata): Observable<Response[]> {
	  let headers = new Headers({ 'Content-Type': 'application/json' });
	  let options = new RequestOptions({ headers: headers });

	  //console.log(userdata);
	  return this.http.post(url, userdata, options)
	    .map((res: Response) => res.json()
	    )
	    .catch(this.handleErrorObservable);
	}

	private handleErrorObservable(error: Response | any) {
	  //console.log("Error");
	  if (error.status == 401) {
	    window.location.href="./#/login";
	  }
	  return Observable.throw(JSON.parse(error._body).errorMessage || error);
	}

	private setHeaders(): Headers {
	  const headersConfig = {
	    'Content-Type': 'application/json'
	  };
	  return new Headers(headersConfig);
	}

}
