import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { Login } from '../../models/login';
import { ApiUrls } from '../../models/api-urls';
import { Global } from '../../models/global'
import 'rxjs/Rx';
import { from } from 'rxjs/observable/from';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	loadingAnimation = false;

	loginForm: FormGroup;
	userdata: any = {};
	login: Login;
	errorMessageBox;
	loader;
	errorMessage;
	emailError;
	passwordError;

	constructor(
		fb: FormBuilder,
		private http: Http,
		private zone: NgZone,
		private router: Router,
		public global: Global,
	) {
		this.loginForm = fb.group({
			'email': [null, Validators.required],
			'password': [null, Validators.required]
		})
	}

	ngOnInit() {
		localStorage.clear();
		this.clearGlobal();
	}

	clearGlobal() {
		this.global.profilePic = '';
		this.global.profileName = '';
		this.global.activatingEmail = '';
		this.global.categoryPostList = undefined;
		this.global.categoryId = '';
		this.global.categoryInfo = undefined;
		this.global.categoryForm = undefined;
		this.global.categoryPostCheck = undefined;
		this.global.catIvalid = true;
		this.global.homepage_post = '';
	}

	emailValidation(event) {
		let newEmail = event.target.value;
		const emailCond = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

		////console.log(newEmail);

		if (newEmail.length > 0) {
			if (!newEmail.match(emailCond)) {
				//console.log('email not valid');
				this.zone.run(() => {
					// this.emailData = 'Please enter a valid email address';
					this.emailError = 'Please enter a valid email address';
					const email = document.getElementById('email-container');
					email.classList.add("error");
				});
			} else {
				this.zone.run(() => {
					this.emailError = "";
					const email = document.getElementById('email-container');
					email.classList.remove("error");
				});
			}
		} else {
			this.zone.run(() => {
				this.emailError = "";
				const email = document.getElementById('email-container');
				email.classList.remove("error");
			});
		}

		if (newEmail.length <= 0) {
			this.zone.run(() => {
				this.errorMessageBox = "";
				this.emailError = '';
				const email = document.getElementById('email-container');
				email.classList.remove("error");
			});
		}
	}

	onLogin(userdata) {
		//console.log(userdata);
		this.errorMessage = '';
		this.emailError = '';
		this.passwordError = '';
		this.loader = 'show';
		const rqstData =
		{
			"username": userdata.email,
			"password": userdata.password
		};

		if ((userdata.email == '') || (userdata.email == undefined)) {
			this.loader = '';
			const email = document.getElementById('email-container');
			email.classList.add("error");
			this.emailError = 'Please enter a username';
			this.errorMessage = 'have error';
		}

		if ((userdata.password == '') || (userdata.password == undefined)) {
			this.loader = '';
			const password = document.getElementById('password-container');
			password.classList.add("error");
			this.passwordError = 'Please enter a password';
			this.errorMessage = 'have error';
		}

		if (this.errorMessage != '') {
			return false;
		}

		const result = this.callService(ApiUrls.loginUrl, rqstData).subscribe((json: Object) => {
			this.login = new Login().fromJSON(json);
			if (this.login.status === 'Success') {
				//console.log(this.login);
				localStorage.setItem('access_token', this.login.access_toke);

				this.router.navigateByUrl('/');
				return;
			} else {
				this.zone.run(() => {
					//console.log(this.login.message);
					this.loader = "";
					this.passwordError = this.login.message;

					const email = document.getElementById('email-container');
					email.classList.add("error");
					const password = document.getElementById('password-container');
					password.classList.add("error");
					this.errorMessage = 'have error';

				});
			}

		}, error => {
			this.errorMessage = <any>error;
		});

		return;
	}


	/**
 	 * api calling
  	*/
	callService(url, userdata): Observable<Response[]> {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });

		//console.log(userdata);
		return this.http.post(url, userdata, options)
			.map((res: Response) => res.json()
			)
			.catch(this.handleErrorObservable);
	}

	private handleErrorObservable(error: Response | any) {
		//console.log("Error");
		if (error.status == 401) {
			window.location.href = "./#/login";
		}
		return Observable.throw(JSON.parse(error._body).errorMessage || error);
	}

	private setHeaders(): Headers {
		const headersConfig = {
			'Content-Type': 'application/json'
		};
		return new Headers(headersConfig);
	}

}
