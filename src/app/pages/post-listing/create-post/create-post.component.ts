import { Component, OnInit, Inject, NgZone, Output, Input, EventEmitter, ElementRef } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { OwnDetails } from '../../../models/own-details';
import { ApiUrls } from '../../../models/api-urls';
import { PostCategories } from '../../../models/post/post-categories';
import { CreatePost } from '../../../models/post/create-post';
import { WebsiteData } from '../../../models/post/website-data';
import 'rxjs/Rx';
import { Global } from '../../../models/global';


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

	static temp: CreatePostComponent;

	errorMessageBox;
	loader;
	isAddclass = false;
	errorMessage;
	access_token: string = localStorage.getItem('access_token');
	ownDetails: OwnDetails;
	postCategories: PostCategories;
	createPostSuccess: CreatePost;
	websiteData: any = {}
	profileImage: string = '';
	listPostCategories;
	postForm: FormGroup;
	userdata: any = {};
	mediaUrl: string = ApiUrls.mediaUrl;
	uploadFilename = 'Choose file';
	isPostReady = true;
	isPreviewActive = false;

	currentUrl: string = '';

	uploadFileBase64;
	uploadFileType;
	postType: string = '';
	@Output() fnSuccessOutput = new EventEmitter<string>();
	@Input() categoryName;

	webTitle: string = '';
	webIcon: string = '';
	webLink: string = '';

	constructor(
		fb: FormBuilder, 
		private http:Http, 
		private zone: NgZone, 
		private router: Router, 
		private elRef: ElementRef,
		public global: Global
	) {
		CreatePostComponent.temp = this;
		this.postForm = fb.group({
	      	'post': [null, Validators.required],
	       	'categoryId': [null, Validators.required]
	    });
	}
	addclassTrue() {
		this.isAddclass = true;
		this.elRef.nativeElement.querySelector('form').reset();
	}
	addclassFalse() {
		this.isAddclass = false;
	}

	ngOnInit() {
		this.getOwnProfile();
		this.getCategories();
		this.getCurrentPage();
		setTimeout(() => {
			this.elRef.nativeElement.querySelector('form').reset();
		}, 500);
	}

	getCurrentPage(){
		let currentUrl = this.router.url;
		let lastIndex = currentUrl.lastIndexOf('/');
		lastIndex = lastIndex - 1;
		this.currentUrl = currentUrl.substr(1, lastIndex);
	}

	getOwnProfile(){
		const rqstData = '';
		const result = this.callService(ApiUrls.ownProfile, rqstData).subscribe((json: Object) => {
         this.ownDetails = new OwnDetails().fromJSON(json);
         if (this.ownDetails.status === 'success') {
         	this.printOwnDetails(this.ownDetails.data);
           	return;
         }else {
         	this.zone.run(() => {
		     	
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}

	printOwnDetails(data){
		this.zone.run(() =>{
			this.profileImage = data.profile_photo;
		});
	}

	getCategories(){
		const rqstData = '';
		const result = this.callService(ApiUrls.postCategory, rqstData).subscribe((json: Object) => {
         this.postCategories = new PostCategories().fromJSON(json);
         //console.log(this.postCategories);
         if (this.postCategories.status === 'success') {
         	this.zone.run(() => {
         		this.listPostCategories = this.postCategories.data;
         	});
           	return;
         }else {
         	this.zone.run(() => {
		     	
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}

	uploadFileShowing(event) {
    	const file_a = event.srcElement.files[0];
    	////console.log(file_a);
    	const fileName_str = file_a.name;
    	const fileType_str = file_a.type;
    	let lastIndex = fileType_str.lastIndexOf('/');
    	lastIndex++;
    	const baseTpeLength = fileType_str.length;
    	const fileType = fileType_str.substring(lastIndex, baseTpeLength);

    	lastIndex--;
    	const postType = fileType_str.substring(0, lastIndex);
    	
		this.getBase64(file_a, this.saveFileArray, fileType, postType);
		this.uploadFilename = this.elRef.nativeElement.querySelector('input#inputGroupFile01').value;
		this.uploadFilename = this.uploadFilename.replace(/.*[\/\\]/, '');
		if(this.elRef.nativeElement.querySelector('input#inputGroupFile01').value != '') {
			this.isPostReady = false;
		} else{
			this.isPostReady = true;
		}
	}
	keyUp() {
		if(this.elRef.nativeElement.querySelector('textarea').value != '') {
			this.isPostReady = false;
			this.isAddclass = true;
		} else{
			this.isPostReady = true;
		}
		// removing smiley's
		setTimeout(() => {
			if (this.userdata.post){
			this.userdata.post = this.userdata.post.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '&#9679; ');
			}
		}, 100);
	}
	//   reset 
	
  	getBase64(file, callback, fileType, postType) {
	    //console.log(fileType);
	    var reader = new FileReader();
	    reader.readAsDataURL(file);
	    reader.onload = function () {
	      let base64Value = reader.result;
	      let lastIndex = base64Value.lastIndexOf(',');
	      lastIndex++;
	      const base64Length = base64Value.length;
	      const converted64 = base64Value.substring(lastIndex, base64Length);
		  
	      callback(converted64, fileType, postType);
	    };
	    reader.onerror = function (error) {
	      //console.log('Error: ', error);
	    };
	}

	saveFileArray(base64, fileType, postType){
		CreatePostComponent.temp.uploadFileBase64 = base64;
		CreatePostComponent.temp.uploadFileType = fileType;
		CreatePostComponent.temp.postType = postType;
	}


	createPost(postdata){
		
		this.errorMessage = '';
		
		if(this.currentUrl != 'category'){
			if((postdata.categoryId == undefined) || (postdata.categoryId == null)){
				const categoryId = document.getElementById('category-id-container');
				categoryId.classList.add('error');
				this.errorMessage = 'Please choose a category';
			}
		}

		if(this.errorMessage != ''){
			return false;
		}

		if(this.postType == ''){
			this.postType = 'text';
		}

		//console.log(this.uploadFileBase64);
		if((postdata.post == undefined) && (this.uploadFileBase64 == undefined)){
			this.postForm.reset();
			this.fnSuccessOutput.emit('complete');
			return;
		}

		this.loader = true;
		this.isAddclass = false;
		this.uploadFilename = 'Choose file';

		let categoryId;

		if(this.currentUrl == 'category'){
			categoryId = this.global.categoryId;
		}else{
			categoryId = postdata.categoryId;
		}
		if (postdata.post != null) {
			postdata.post = postdata.post.replace(/(?:\r\n|\r|\n)/g, '<br>');
		}
		const post_a = JSON.stringify(
			{
				categoryId: categoryId,
				postTitle: postdata.post,
				fileType: this.uploadFileType,
				postType: this.postType,
				fileData: this.uploadFileBase64,
				webTitile: this.webTitle,
				webIcon: this.webIcon,
				webLink: this.webLink
			}
		);

		//console.log(post_a);

		const result = this.callService(ApiUrls.createPost, post_a).subscribe((json: Object) => {
         this.createPostSuccess = new CreatePost().fromJSON(json);
         //console.log(this.createPostSuccess);
         if (this.createPostSuccess.status === 'Success') {
         	this.zone.run(() => {
         		this.postForm.reset();
				this.websiteData = {};
				this.webIcon = '';
				this.webLink = '';
				this.webTitle = '';
				this.fnSuccessOutput.emit('complete');
         	})
			 this.loader = false;
           	return;
         }else {
         	this.zone.run(() => {
		     	this.loader = false;
		     	this.errorMessage = this.createPostSuccess.message;
         	});
         }

        }, error => {
          this.errorMessage = <any>error;
        });
	}

	fetchWebsiteDetails(){
		// const element = <HTMLInputElement>document.getElementById('post-title');
		setTimeout(() => {
			const element = this.elRef.nativeElement.querySelector('textarea');
			const value = element.value;
			//console.log(value);

			const rqstData = JSON.stringify(
				{
					websiteLink: value
				}
			);

			const result = this.callService(ApiUrls.fetchWebsiteDetails, rqstData).subscribe((json: Object) => {
				this.websiteData = new WebsiteData().fromJSON(json);
				//console.log(this.websiteData);
				if (this.websiteData.status === 'success') {
					this.isPreviewActive = true;
					//console.log(this.websiteData.data);
					const data = this.websiteData.data;
					this.zone.run(() => {
						this.webTitle = data['title'];
						this.webIcon = data['icon'];
						this.webLink = data['link'];
						this.postType = 'link';
						// this.fnSuccessOutput.emit('complete');
						// this.router.navigateByUrl('/home');
					})
					this.loader = false;
					return;
				} else {
					this.isPreviewActive = false;
					this.zone.run(() => {
						this.loader = false;
						this.errorMessage = this.createPostSuccess.message;
					});
				}

			}, error => {
				this.errorMessage = <any>error;
			});
		}, 10);
	}
	resetWebsiteDetailes() {
		this.websiteData = {};
		this.webIcon = '';
		this.webLink = '';
		this.webTitle = '';
		this.isPreviewActive = false;
	}

	/**
 	 * api calling
  	*/
	callService(url, userdata): Observable<Response[]> {
	  let headers = new Headers(
	  		{ 
	  			'Content-Type': 'application/json',
	  			'token': this.access_token 
	  		}
	  	);
	  let options = new RequestOptions({ headers: headers });

	  //console.log(userdata);
	  return this.http.post(url, userdata, options)
	    .map((res: Response) => res.json()
	    )
	    .catch(this.handleErrorObservable);
	}

	private handleErrorObservable(error: Response | any) {
	  //console.log("Error");
	  if (error.status == 401) {
	    window.location.href="./#/login";
	  }
	  return Observable.throw(JSON.parse(error._body).errorMessage || error);
	}

	private setHeaders(): Headers {
	  const headersConfig = {
	    'Content-Type': 'application/json'
	  };
	  return new Headers(headersConfig);
	}
}
