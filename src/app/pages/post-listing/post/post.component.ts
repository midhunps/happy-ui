import { Component, OnInit, OnDestroy, ViewEncapsulation, ElementRef, Output, EventEmitter, Inject, NgZone, HostListener, Input, ViewChild} from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { OwnDetails } from '../../../models/own-details';
import { ApiUrls } from '../../../models/api-urls';
import { PostCategories } from '../../../models/post/post-categories';
import { CreatePost } from '../../../models/post/create-post';
import { PostList } from '../../../models/post/post-list';
import { LikeSuccess } from '../../../models/post/like-success';
import { PostComment } from '../../../models/post/post-comment';
import { StatusClass } from '../../../models/status-class';
import {Global} from '../../../models/global';

import 'rxjs/Rx';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PostComponent implements OnInit {

  static temp: PostComponent;
  closeResult: string;
  toggleCommented = false;
  likeCount = 12;
  toggleLike = false;
  liketext = ' Like';
  toggleFollowed = false;
  followtext = ' Follow';
  private NgbModalRef: NgbModalRef;
  private NgbModalRefLikeList: NgbModalRef;

  errorMessageBox;
  loader;
  errorMessage;
  access_token: string = localStorage.getItem('access_token');
  postList:PostList;
  likeSuccess:LikeSuccess;
  postComment:PostComment;
  statusClass:StatusClass;
  walpostList;
  mediaUrl: string = ApiUrls.mediaUrl;
  commentForm: FormGroup;
  isLoadingComment = false;
  isCommentLoad = false;
  proPic;
  isHasContent = false;
  deleting = false;
  videoElement;
  docOffsetTop;
  videoElementOffset;
  windowBottom;
  windowTop = 0;
  position = document.documentElement.scrollTop;
  scrolling;
  uploadFilename;
  uploadFile;
  openCommentList;

  uploadFileBase64: string = '';
  uploadFileType: string = '';
  commentType: string = '';
  selectedPostId: string = '';

  @Output() clsModal = new EventEmitter<string>();
  @Input() post: any;
  @Input() index: any;

  @Output() likeFnOutput = new EventEmitter<any>();
  @Output() unLikeFnOutput = new EventEmitter<any>();
  @Output() commentFnOutput = new EventEmitter<any>();
  @Output() deleteFnOutput = new EventEmitter<any>();
  @Output() editFnOutput = new EventEmitter<any>();
  @Output() followFnOutput = new EventEmitter<any>();
  @Output() unFollowFnOutput = new EventEmitter<any>();
  @Output() gotoProfileOutput = new EventEmitter<any>();
  
  @ViewChild('commentItem') child;

  profile_pic: string;
  post_Text;
  postURL;
  
  constructor(
    private modalService: NgbModal, 
    private elRef: ElementRef, 
    fb: FormBuilder, 
    private http:Http, 
    private zone: NgZone, 
    public router: Router, 
    public global: Global
  ) {
    this.profile_pic = global.profilePic;
    PostComponent.temp = this;
  }
  
  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
    this.post_Text = this.post.post.replace(/<br\s*\/?>/mg,"\n");
    this.postURL = location.origin + '/#/post/' + this.post.id;
  }
  
  scroll = (): void => {
    this.videoElement = this.elRef.nativeElement.querySelector('video');
    if (this.videoElement != null) {
      this.docOffsetTop = document.documentElement.scrollTop;
      this.videoElementOffset = this.elRef.nativeElement.querySelector('div.post-wrap').offsetTop;
      this.videoElementOffset = this.videoElementOffset + this.elRef.nativeElement.querySelector('div.head').offsetHeight;
      this.videoElementOffset = this.videoElementOffset + this.videoElement.offsetHeight / 2;
      this.windowBottom = window.innerHeight;
      // downwards or upwards
      this.scrolling = document.documentElement.scrollTop;
      if (this.scrolling > this.position) {
        // //console.log("scrolling downwards");
        if (this.docOffsetTop >= this.videoElementOffset) {
          this.videoElement.pause();
        }
      } else {
        // //console.log("scrolling upwards");
        if (this.docOffsetTop + this.windowBottom <= this.videoElementOffset) {
          this.videoElement.pause();
        }
      }
      this.position = this.scrolling;

      // //console.log(this.videoElement.offsetHeight);
      // //console.log(this.videoElementOffset);
    }
  }
  
  copied() {
    alert('link copied');
  }
  // toggleCommented
  toggleComment() {
    this.toggleCommented = !this.toggleCommented;
    setTimeout(() => {
      this.elRef.nativeElement.querySelector('textarea.mainComment').focus();
    }, 100);
  }
  onChange($event) {
    if(this.elRef.nativeElement.querySelector('textarea.mainComment').value === '') {
      this.isHasContent = false;
    } else {
      this.isHasContent = true;
    }
  }

  uploadFileShowing(event) {
      //console.clear();
      //console.log(event);
      const file_a = event.srcElement.files[0];
      this.selectedPostId = event.srcElement.name;
      ////console.log(file_a);
      const fileName_str = file_a.name;
      const fileType_str = file_a.type;
      let lastIndex = fileType_str.lastIndexOf('/');
      lastIndex++;
      const baseTpeLength = fileType_str.length;
      const fileType = fileType_str.substring(lastIndex, baseTpeLength);

      lastIndex--;
      const commentType = fileType_str.substring(0, lastIndex);
      this.getBase64(file_a, this.saveFileArray, fileType, commentType);
      this.uploadFile = this.elRef.nativeElement.querySelector('input[type="file"]').value;
      this.uploadFilename = this.uploadFile.replace(/.*[\/\\]/, '');
      if (this.uploadFilename != '') {
        this.isHasContent = true;
      }
  }

    removeFile(){
      this.elRef.nativeElement.querySelector('input[type="file"]').value = '';
      this.isHasContent = false;
      PostComponent.temp.uploadFileBase64 = '';
      PostComponent.temp.uploadFileType = '';
      PostComponent.temp.commentType = '';
      this.uploadFilename = '';
    }


    getBase64(file, callback, fileType, commentType) {
      //console.log(fileType);
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
      let base64Value = reader.result;
      let lastIndex = base64Value.lastIndexOf(',');
      lastIndex++;
      const base64Length = base64Value.length;
      const converted64 = base64Value.substring(lastIndex, base64Length);
      //console.log(converted64);
      //console.log(commentType);
    
    
      callback(converted64, fileType, commentType);
      };
      reader.onerror = function (error) {
        //console.log('Error: ', error);
      };
    }

    saveFileArray(base64, fileType, commentType){
      PostComponent.temp.uploadFileBase64 = base64;
      PostComponent.temp.uploadFileType = fileType;
      PostComponent.temp.commentType = commentType;
    }


  deletePost(postId){
    this.deleting = true;
    const rqstData = JSON.stringify(
      {
        postId: postId,
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.deletePost, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         //console.log(this.statusClass);
         if (this.statusClass.status === 'success') {
           // this.walpostList = this.postList.data;
          //  this.deleteFnOutput.emit(0);
          this.elRef.nativeElement.querySelector('div.post-wrap').remove();
             return;
         }else {
           this.zone.run(() => {
           
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });
  }
  
  openEdit(postId){
    const element = document.getElementById('edit-group-'+postId);
    element.classList.remove("hide");
  }
  updatePost(postId, index){
    const element = <HTMLInputElement>document.getElementById('edit-text-'+postId);
    const postValue = element.value.replace(/(?:\r\n|\r|\n)/g, '<br>');

    const update_a = {
      postId: postId,
      postTitle: postValue,
      index: index
    }

    const elementContainer = document.getElementById('edit-group-'+postId);
    
    //console.log(postTitle);

    const rqstData = JSON.stringify(
      {
        postId: postId,
        postTitle: postValue
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.updatePost, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         //console.log(this.statusClass);
         if (this.statusClass.status === 'success') {
           elementContainer.classList.add("hide");
           // this.walpostList = this.postList.data;
           this.editFnOutput.emit(update_a);
             return;
         }else {
           this.zone.run(() => {
           
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });
  }

  // like
  likeFun(likes, index, status, postId) {
    //console.log(likes);
    //console.log(index);
    const like_a = {
        like_count : likes,
        index : index
      };
    
    this.toggleLike = status;
    this.zone.run(() => {
      if (this.toggleLike === true) {
        likes = parseInt(likes);
        this.post.like_count = likes + 1;
        this.post.is_liked = 1;
        // this.likeFnOutput.emit(like_a);
      } else {
        likes = parseInt(likes);
        this.post.like_count = likes - 1;
        this.post.is_liked = 0;
        // this.unLikeFnOutput.emit(like_a);
      }
    });

    this.likeAndUnlke(postId);
  }

  detailedLike(event){
    //console.clear();
    //console.log(event);
    // this.likeFnOutput.emit(event);
    this.zone.run(() => {
      const likes = parseInt(event.like_count);
      this.post.like_count = likes + 1;
      this.post.is_liked = 1;
    });
  }

  detailedUnlike(event){
    // this.unLikeFnOutput.emit(event);
    this.zone.run(() => {
      const likes = parseInt(event.like_count);
      this.post.like_count = likes - 1;
      this.post.is_liked = 0;
    });
  }

  detailedComment(event){
    //console.clear();
    //console.log(event);
    if (event.commentType === 'new comment'){
      const postId = event.postId;
      this.child.getComments(postId);
      this.post.comment_count = parseInt(this.post.comment_count) + 1;
    }else{
      const postId = event.postId;
      this.child.getComments(postId);
      this.post.comment_count = parseInt(this.post.comment_count) - 1;
    }
  }

  /**
   * follow
   */
  followFun(folows, index, status, postId) {
    //console.log(folows);
    //console.log(index);
    const follow_a = {
        follow_count : folows,
        index : index
      };
    
    this.toggleFollowed = status;
    //console.log(this.toggleFollowed);
    this.zone.run(() => {
      if (this.toggleFollowed === true) {
        folows = parseInt(folows);
        this.post.follow_count = folows + 1;
        this.post.is_followed = 1;
        // this.followFnOutput.emit(follow_a);
        // this.followtext = ' Followed';
      } else {
        folows = parseInt(folows);
        this.post.follow_count = folows - 1;
        this.post.is_followed = 0;
        // this.unFollowFnOutput.emit(follow_a);
        // this.followtext = ' Follow';
      }
    });

    this.followAndUnfollow(postId);
  }

  commentLoading() {
    this.isCommentLoad = !this.isCommentLoad;
  }
  
  followFnDetailed(event) {
    const follows = parseInt(event.follow_count);
    const index = event.index;
    this.post.follow_count = follows + 1;
    this.post.is_followed = 1;
    /* this.walpostList[index].follow_count = follows + 1;
    this.walpostList[index].is_followed = 1; */
  }

  unfollowFnDetailed(event) {
    const follows = parseInt(event.follow_count);
    const index = event.index;
    this.post.follow_count = follows - 1;
    this.post.is_followed = 0;
    /* this.walpostList[index].follow_count = follows - 1;
    this.walpostList[index].is_followed = 0; */
  }

  
  followAndUnfollow(postId){

    const rqstData = JSON.stringify(
      {
        postId: postId,
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.followPost, rqstData).subscribe((json: Object) => {
         this.likeSuccess = new LikeSuccess().fromJSON(json);
         //console.log(this.likeSuccess);
         if (this.likeSuccess.status === 'success') {
           // this.walpostList = this.postList.data;
             return;
         }else {
           this.zone.run(() => {
            this.router.navigateByUrl('login');
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });

  }


  likeAndUnlke(postId){

    const rqstData = JSON.stringify(
      {
        postId: postId,
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.likePost, rqstData).subscribe((json: Object) => {
         this.likeSuccess = new LikeSuccess().fromJSON(json);
         //console.log(this.likeSuccess);
         if (this.likeSuccess.status === 'success') {
           // this.walpostList = this.postList.data;
             return;
         }else {
           this.zone.run(() => {
            this.router.navigateByUrl('login');
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });

  }


  // modals
  openLg(content, postId, index) {
    // alert(index);
    localStorage.removeItem('postId');
    localStorage.setItem('postId', postId);
    this.global.detailedPostId  = postId;
    this.global.detailedPostIndex = index;
    this.NgbModalRef = this.modalService.open(content, { size: 'lg' });
  }
  openSm(content, postId) {
    localStorage.removeItem('postId');
    localStorage.setItem('postId', postId);
    this.NgbModalRefLikeList = this.modalService.open(content, { size: 'sm' });
  }
  // for closing opened detailed post modal
  modalClose() {
    this.NgbModalRef.close();
  }
  likeModalClose() {
    this.NgbModalRefLikeList.close();
  }

  goProfileFromComment(event){
    this.goToProfile(event);
  }

  goToProfile(username){
    let currentUrl = this.router.url;
    const urlLength = currentUrl.length;
    currentUrl = currentUrl.substr(1, 11);

    if (currentUrl === 'profileview'){
      this.gotoProfileOutput.emit(username);
    }

    // //console.log(currentUrl);
    // this.router.navigateByUrl('profileview/'+username)
    this.router.navigate(['profileview', username]);
  }

  goToCategory(categoryId){
    this.global.categoryId = categoryId;
    this.global.categoryForm = undefined;
    this.global.categoryInfo = undefined;
    this.global.categoryPostList = undefined;
    this.router.navigate(['category', categoryId])
  }

  postComments(postId, index, count){
    const element = <HTMLInputElement>document.getElementById('comment'+postId);
    let comment = element.value;
    //console.log(comment);

    let  file = '';
    let  fileType = '';
    let  commentType = '';

    if((postId == this.selectedPostId) && PostComponent.temp.uploadFileType != ''){
      file = PostComponent.temp.uploadFileBase64
      fileType = PostComponent.temp.uploadFileType
      commentType = PostComponent.temp.commentType
    }else{
      file = '';
      fileType = '';
      commentType = 'text';
    }

    const rqstData = JSON.stringify(
      {
        postId: postId,
        comment: comment,
        commentType: commentType,
        fileData: file,
        fileType: fileType
      }
    );

    const comment_a = {
      index: index,
      postId: postId,
      comment: comment,
      count: count
    };
    this.isLoadingComment = true;
    //console.log(rqstData);
    const result = this.callService(ApiUrls.commentPost, rqstData).subscribe((json: Object) => {
         this.postComment = new PostComment().fromJSON(json);
         //console.log(this.postComment);
         this.isLoadingComment = false;
         element.value = '';
         this.toggleCommented = false;
         if (this.postComment.status === 'Success') {
             
             this.child.getComments(postId);
             this.post.comment_count = parseInt(this.post.comment_count) + 1;
            
            file = ''; 
            fileType = ''; 
            commentType = ''; 
            PostComponent.temp.uploadFileBase64 = '';
            PostComponent.temp.uploadFileType = '';
            PostComponent.temp.commentType = '';

             return;
         }else {
           this.zone.run(() => {
            this.router.navigateByUrl('login');
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });

  }

  deleteCommentSuccess(event){
    let comment_count = parseInt(this.post.comment_count);
    comment_count--;
    //console.log(comment_count);
    this.zone.run(() => {
      this.post.comment_count = comment_count;
    });
    
  }


  /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }
}
