import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentTextComponent } from './comment-text.component';

describe('CommentTextComponent', () => {
  let component: CommentTextComponent;
  let fixture: ComponentFixture<CommentTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
