import { Component, OnInit, Input, ElementRef, OnChanges } from '@angular/core';

@Component({
  selector: 'app-comment-text',
  templateUrl: './comment-text.component.html',
  styleUrls: ['./comment-text.component.scss']
})
export class CommentTextComponent implements OnInit, OnChanges {

  @Input() text;
  @Input() popup;
  textSplit;
  mainText = '';
  trimText = '';
  fullText;
  trim = false;
  seeMore = false;
  brLength = 0;
  toggle = true;
  constructor(private elRef: ElementRef) { }
  // divHeight;

  ngOnInit() {
    if (!this.popup) {
      this.initializer();
    }
  }
  ngOnChanges() {
    if (this.popup) {
      this.initializer();
    }
  }
  initializer() {
    this.mainText = '';
    this.trimText = '';
    const spaceBr1 = this.text.replace(/>/g, '> ');
    const spaceBr = spaceBr1.replace(/</g, ' <');
    this.textSplit = spaceBr.split(" ");
    this.textSplit.forEach(element => {
      if(new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?").test(element)) {
        this.mainText += '<a href="' + element + '" target="_blank">' + element + '</a> '
      } else {
        this.mainText += element + ' ';
      }
    });
    const mainSpilt = this.mainText.split(" ");
    const maxChar = 300;
  // if (this.mainText.length >= maxChar) {
    mainSpilt.forEach(element => {
      if (element.includes('<br') && this.seeMore == false) {
        this.brLength++;
        if (this.brLength <= 5 && this.trimText.length <= maxChar) {
          this.trimText += element + ' ';
        } else {
          this.seeMore = true;
          
        }
      } else if (this.trimText.length <= maxChar && this.seeMore == false) {
        this.trimText += element + ' ';
      } else {
        if (!this.seeMore) {
          this.trimText += '...';
        }
        this.seeMore = true;
      }
    });
  }
}
