import { Component, OnInit, ElementRef, Output, EventEmitter, Input, Inject, NgZone, HostListener } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiUrls } from '../../../../models/api-urls';
import { ListComment } from '../../../../models/post/list-comment';
import { StatusClass } from '../../../../models/status-class';
import { Global } from '../../../../models/global'


@Component({
  selector: 'app-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.scss']
})

export class CommentItemComponent implements OnInit {
  toggleRplay = false;
  access_token: string = localStorage.getItem('access_token');
  listComment:ListComment;
  statusClass:StatusClass;
  commentList;
  errorMessage;
  currentTimezone: string = '';
  mediaUrl: string = ApiUrls.mediaUrl;

  constructor(
    private elRef: ElementRef, 
    private http:Http, 
    private zone: NgZone, 
    private router: Router,
    private global: Global
  ) { }
  @Output() clsModal = new EventEmitter<string>();
  @Output() loading = new EventEmitter<string>();
  @Output() gotoProfileOutput = new EventEmitter<any>();
  @Output() deleteCommentOutput = new EventEmitter<any>();
  @Input() postId: number;
  @Input() post: any;
  ngOnInit() {
   
    //console.log(this.post);
    this.getComments(this.postId);
  }

  getTimezone() {
    let date = new Date();
    const string = date.toString();
    const lastIndex = string.lastIndexOf('+');
    const stringLength = string.length;
    let timezone = string.substring(lastIndex, stringLength);
    const timezoneLength = timezone.lastIndexOf(' (');
    timezone = timezone.substring(0, timezoneLength);
    const timezone_a = timezone.split('', 5);
    this.currentTimezone = timezone_a[0] + timezone_a[1] + timezone_a[2] + ":" + timezone_a[3] + timezone_a[4];
    this.global.timezone = this.currentTimezone;

    //console.clear();
    //console.log(this.currentTimezone);
  }

  goToProfile(username){
    this.gotoProfileOutput.emit(username);
  }

  // for closing opened detailed post modal
  modalClose() {
    this.clsModal.emit('complete');
  }
  // toggleCommented
  toggleRplyComment() {
    this.toggleRplay = !this.toggleRplay;
    setTimeout(() => {
      this.elRef.nativeElement.querySelector('textarea').focus();
    }, 100);
  }

  showCommentEdit(commentId){
    const element = document.getElementById('edit-comment-' + commentId);
    element.classList.remove('hide');
  }

  editComment(commentId, postId){
    const element = <HTMLInputElement>document.getElementById('edit-com-text-'+commentId);
    const comment = element.value;

    const rqstData = JSON.stringify({
      comId: commentId,
      newComment: comment
    });

    const result = this.callService(ApiUrls.editComment, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         //console.log(this.statusClass);
         if (this.statusClass.status === 'success') {
             this.getComments(postId);
             return;
         }else {
          this.loading.emit('complete');
           this.zone.run(() => {
           
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });

  }

  getComments(postId){
    this.loading.emit('complete');
    const rqstData = JSON.stringify(
      {
        postId: postId,
        timezone: this.global.timezone
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.getComments, rqstData).subscribe((json: Object) => {
         this.listComment = new ListComment().fromJSON(json);
         //console.log(this.listComment);
         if (this.listComment.status === 'success') {
             this.commentList = this.listComment.data;
             this.loading.emit('complete');
             return;
         }else {
           this.commentList = [];
          this.loading.emit('complete');
           this.zone.run(() => {
           
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });
  }

  deleteComment(comId, post_id){
    const rqstData = JSON.stringify(
      {
        comId: comId
      }
    );

     const result = this.callService(ApiUrls.deleteComment, rqstData).subscribe((json: Object) => {
         this.statusClass = new StatusClass().fromJSON(json);
         //console.log(this.statusClass);
         if (this.statusClass.status === 'success') {
             this.getComments(post_id);
           this.deleteCommentOutput.emit(post_id);
             return;
         }else {
          this.loading.emit('complete');
           this.zone.run(() => {
           
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });

  }



    /**
    * api calling
    */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
