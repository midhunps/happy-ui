import { Component, OnInit, ViewEncapsulation, NgZone, EventEmitter, Output } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { PostLikes } from '../../../../models/post/post-likes';
import { ApiUrls } from '../../../../models/api-urls';
import 'rxjs/Rx';

@Component({
  selector: 'app-liked-list',
  templateUrl: './liked-list.component.html',
  styleUrls: ['./liked-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LikedListComponent implements OnInit {

  access_token: string = localStorage.getItem('access_token');
  likedPersons;
  likedPersonsLength;
  postId: string = localStorage.getItem('postId');
  PostLikes:PostLikes;
  errorMessage;
  mediaUrl: string = ApiUrls.mediaUrl;
  loader = true;
  currentTimezone: string = "";
  @Output() likeClsModal = new EventEmitter<string>();
  constructor(private modalService: NgbModal, fb: FormBuilder, private http:Http, private zone: NgZone, private router: Router) { }

  ngOnInit() {
    this.getTimezone();
  	this.getLikes(this.postId);
  }

  getTimezone() {
    let date = new Date();
    const string = date.toString();
    const lastIndex = string.lastIndexOf('+');
    const stringLength = string.length;
    let timezone = string.substring(lastIndex, stringLength);
    const timezoneLength = timezone.lastIndexOf(' (');
    timezone = timezone.substring(0, timezoneLength);
    const timezone_a = timezone.split('', 5);
    this.currentTimezone = timezone_a[0] + timezone_a[1] + timezone_a[2] + ":" + timezone_a[3] + timezone_a[4];

    //console.clear();
    //console.log(this.currentTimezone);
  }

  getLikes(postId){
    const rqstData = JSON.stringify(
      {
        postId: postId,
        timezone: this.currentTimezone
      }
    );
    const result = this.callService(ApiUrls.getLikes, rqstData).subscribe((json: Object) => {
         this.PostLikes = new PostLikes().fromJSON(json);
         //console.log(this.PostLikes);
         if (this.PostLikes.status === 'success') {
             this.zone.run(() => {
               this.likedPersons = this.PostLikes.data;
               this.loader = false;
             });
             
             /*for(let i=0; i<postData.length; i++){

             }*/
             // return;
         }else {
           this.zone.run(() => {
           
           });
         }

      }, error => {
        this.errorMessage = <any>error;
      });
  }
  // for closing opened detailed post modal
  modalClose() {
    this.likeClsModal.emit('complete');
  }


	/**
	* api calling
	*/
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
        { 
          'Content-Type': 'application/json',
          'token': this.access_token 
        }
      );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href="./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
