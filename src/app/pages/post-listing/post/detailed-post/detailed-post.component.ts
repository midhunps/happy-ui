import { Component, OnInit, ViewEncapsulation, ElementRef, Output, Input, EventEmitter, Inject, NgZone, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, NgForm } from '@angular/forms';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { PostList } from '../../../../models/post/post-list';
import { LikeSuccess } from '../../../../models/post/like-success';
import { PostComment } from '../../../../models/post/post-comment';
import { PostLikes } from '../../../../models/post/post-likes';
import { ListComment } from '../../../../models/post/list-comment';
import { ApiUrls } from '../../../../models/api-urls';
import { Global } from '../../../../models/global';
import { Meta, Title } from '@angular/platform-browser';
import 'rxjs/Rx';

@Component({
  selector: 'app-detailed-post',
  templateUrl: './detailed-post.component.html',
  styleUrls: ['./detailed-post.component.scss']
})
export class DetailedPostComponent implements OnInit {

  static temp: DetailedPostComponent;
  closeResult: string;
  toggleCommented = false;
  likeCount = 12;
  toggleLike = false;
  liketext = ' Like';
  toggleFollowed = false;
  likesWrap = false;
  followtext = ' Follow';
  forFocus = 0;
  loading = true;
  isCommentLoad = false;

  errorMessageBox;
  errorMessage;
  postList: PostList;
  likeSuccess: LikeSuccess;
  postComment: PostComment;
  PostLikes: PostLikes;
  listComment: ListComment;
  walpostList;
  mediaUrl: string = ApiUrls.mediaUrl;
  commentForm: FormGroup;
  access_token: string = localStorage.getItem('access_token');
  wrap = document.getElementsByTagName('ngb-modal-window');
  likedPersons;
  likedPersonsLength;
  commentList;
  lastCommented;
  commentListLength;
  isLoadingComment;
  isHasContent = false;

  postLikeCount: number;
  postCommentCount: number;
  postFollowCount: number;

  @ViewChild('commentItem') child;
  @Input() post: any;

  uploadFilename;
  uploadFile;

  uploadFileBase64: string = '';
  uploadFileType: string = '';
  commentType: string = '';
  selectedPostId: string = '';
  currentTimezone: string = '';
  postURL;
  postURL2;

  @Output() clsModal = new EventEmitter<string>();
  @Output() likeOutput = new EventEmitter<any>();
  @Output() unLikeOutput = new EventEmitter<any>();
  @Output() followFnOutput = new EventEmitter<any>();
  @Output() unFollowFnOutput = new EventEmitter<any>();
  @Output() commentFnOutput = new EventEmitter<any>();

  constructor(
    private modalService: NgbModal,
    private elRef: ElementRef,
    fb: FormBuilder,
    private http: Http,
    private zone: NgZone,
    private router: Router,
    public global: Global,
    private route: ActivatedRoute,
    private meta: Meta,
    private title: Title
  ) {
    DetailedPostComponent.temp = this;
    this.route.params.subscribe(params => { this.post_id = params.postId });
  }

  postId: string = localStorage.getItem('postId');
  post_id;
  // postId = this.global.detailedPostId;
  postIndex = this.global.detailedPostIndex;
  ngOnInit() {
    this.route.params.subscribe(params => { this.post_id = params.postId });
    if (this.global.timezone == undefined) {
      this.getTimezone();
    }
    if (this.post_id == undefined) {
      this.getPostDetails(this.postId);
    } else {
      this.getPostDetails(this.post_id);
    }
    this.getLikes(this.postId);
    this.getComments();
    this.walpostList = { post: '' };
    this.postURL = location.origin + '/#/post/' + this.postId;
    this.postURL2 = location.origin + '/#/post/' + this.post_id;
  }
  getTimezone() {
    let date = new Date();
    const string = date.toString();
    const lastIndex = string.lastIndexOf('+');
    const stringLength = string.length;
    let timezone = string.substring(lastIndex, stringLength);
    const timezoneLength = timezone.lastIndexOf(' (');
    timezone = timezone.substring(0, timezoneLength);
    const timezone_a = timezone.split('', 5);
    this.currentTimezone = timezone_a[0] + timezone_a[1] + timezone_a[2] + ":" + timezone_a[3] + timezone_a[4];
    this.global.timezone = this.currentTimezone;

    //console.clear();
    //console.log(this.currentTimezone);
  }

  // toggleCommented
  toggleComment() {
    this.toggleCommented = !this.toggleCommented;
    setTimeout(() => {
      this.elRef.nativeElement.querySelector('textarea.mainComment').focus();
    }, 100);
  }
  onChange($event) {
    if (this.elRef.nativeElement.querySelector('textarea.mainComment').value === '') {
      this.isHasContent = false;
    } else {
      this.isHasContent = true;
    }
  }
  commentLoading() {
    this.isCommentLoad = !this.isCommentLoad;
  }

  commentDelete(event) {
    const commentOut_a = {
      postId: event,
      commentType: 'delete comment'
    }

    this.commentFnOutput.emit(commentOut_a);
    this.getComments();
  }
  // like
  /*likeFun() {
    this.toggleLike = !this.toggleLike;
    if (this.toggleLike === true) {
      this.likeCount = this.likeCount + 1;
      this.liketext = ' Liked';
    } else {
      this.likeCount = this.likeCount - 1;
      this.liketext = ' Like';
    }
  }*/

  likeFun(likes, status, postId) {
    //console.log(likes);
    likes = parseInt(likes);
    this.toggleLike = status;
    this.zone.run(() => {
      if (this.toggleLike === true) {
        this.walpostList.is_liked = 1;
        this.walpostList.like_count = '';
        this.walpostList.like_count = likes + 1;

        const like_a = {
          like_count: likes,
          index: this.global.detailedPostIndex
        };
        this.likeOutput.emit(like_a);
      } else {
        this.walpostList.is_liked = 0;
        this.walpostList.like_count = '';
        this.walpostList.like_count = likes - 1;

        const like_a = {
          like_count: likes,
          index: this.global.detailedPostIndex
        };

        this.unLikeOutput.emit(like_a);
      }
    });

    this.likeAndUnlke(postId);
  }

  /* followFun() {
    this.toggleFollowed = !this.toggleFollowed;
    if (this.toggleFollowed === true) {
      this.followtext = ' Followed';
    } else {
      this.followtext = ' Follow';
    }
  } */


  /**
   * follow
   */
  followFun(folows, index, status, postId) {
    //console.log(folows);
    //console.log(index);
    const follow_a = {
      follow_count: folows,
      index: index
    };

    this.toggleFollowed = status;
    //console.log(this.toggleFollowed);
    this.zone.run(() => {
      if (this.toggleFollowed === true) {
        folows = parseInt(folows);
        this.walpostList.follow_count = folows + 1;
        this.walpostList.is_followed = 1;
        this.followFnOutput.emit(follow_a);
        // this.followtext = ' Followed';
      } else {
        folows = parseInt(folows);
        this.walpostList.follow_count = folows - 1;
        this.walpostList.is_followed = 0;
        this.unFollowFnOutput.emit(follow_a);
        // this.followtext = ' Follow';
      }
    });

    this.followAndUnfollow(postId);
  }

  // listing like and comment
  likedList() {
    this.likesWrap = !this.likesWrap;
    this.forFocus = this.elRef.nativeElement.querySelector('div.like-wrap').offsetTop;
    this.wrap[0].scrollTop = this.forFocus;
  }
  commentedList() {
    this.forFocus = this.elRef.nativeElement.querySelector('div.comment-wrap').offsetTop;
    this.wrap[0].scrollTop = this.forFocus;
  }
  // for closing opened detailed post modal
  modalClose($event) {
    this.clsModal.emit('complete');
  }

  followAndUnfollow(postId) {

    const rqstData = JSON.stringify(
      {
        postId: postId,
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.followPost, rqstData).subscribe((json: Object) => {
      this.likeSuccess = new LikeSuccess().fromJSON(json);
      //console.log(this.likeSuccess);
      if (this.likeSuccess.status === 'success') {
        // this.walpostList = this.postList.data;
        return;
      } else {
        this.zone.run(() => {
          this.router.navigateByUrl('login');
        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });

  }

  likeAndUnlke(postId) {

    const rqstData = JSON.stringify(
      {
        postId: postId,
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.likePost, rqstData).subscribe((json: Object) => {
      this.likeSuccess = new LikeSuccess().fromJSON(json);
      //console.log(this.likeSuccess);
      if (this.likeSuccess.status === 'success') {
        // this.walpostList = this.postList.data;
        return;
      } else {
        this.zone.run(() => {
          this.router.navigateByUrl('login');
        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });

  }

  getPostDetails(postId) {
    const rqstData = JSON.stringify(
      {
        postId: postId,
        timezone: this.global.timezone
      }
    );
    const result = this.callService(ApiUrls.getPostDetails, rqstData).subscribe((json: Object) => {
      this.postList = new PostList().fromJSON(json);
      //console.log(this.postList);
      if (this.postList.status === 'success') {

        this.walpostList = this.postList.data;
        this.postLikeCount = this.walpostList.like_count;
        this.postFollowCount = this.walpostList.follow_count;
        this.postCommentCount = this.walpostList.comment_count;

        this.loading = false;
        //console.log(this.walpostList);
        this.zone.run(() => {
          this.walpostList = this.postList.data;
        })
        this.setMeta();
        return;
      } else {
        this.zone.run(() => {

        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });
  }
  
  setMeta() {
    if (location.href.indexOf("/post/") > -1) {
      this.meta.addTag({ name: 'description', content: this.walpostList.post.replace(/<br\s*\/?>/mg,"")});
      this.meta.addTag({ property: 'og:type', content: 'article' });
      if (this.walpostList.post_type == 'image') {
        this.meta.addTag({ property: 'og:image', content: this.mediaUrl + this.walpostList.attachment });
      }
      this.meta.addTag({ property: 'og:site_name', content: 'HappyEquities' });
      this.title.setTitle('HappyEquities - ' + this.walpostList.post.replace(/<br\s*\/?>/mg,""));
    }
  }


  getLikes(postId) {
    const rqstData = JSON.stringify(
      {
        postId: postId,
      }
    );
    const result = this.callService(ApiUrls.getLikes, rqstData).subscribe((json: Object) => {
      this.PostLikes = new PostLikes().fromJSON(json);
      //console.log(this.PostLikes);
      if (this.PostLikes.status === 'success') {
        const postData = this.PostLikes.data;
        this.zone.run(() => {
          this.likedPersons = postData[0];
          this.likedPersonsLength = postData.length;
        })

        /*for(let i=0; i<postData.length; i++){

        }*/
        // return;
      } else {
        this.zone.run(() => {
          // this.router.navigateByUrl('login');
        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });
  }

  getComments() {
    const rqstData = JSON.stringify(
      {
        postId: this.postId
      }
    );

    //console.log(rqstData);
    const result = this.callService(ApiUrls.getComments, rqstData).subscribe((json: Object) => {
      this.listComment = new ListComment().fromJSON(json);
      //console.log(this.listComment);
      if (this.listComment.status === 'success') {
        const postData = this.listComment.data;
        this.lastCommented = postData[0];
        this.commentListLength = postData.length;
      } else {
        this.zone.run(() => {
          this.lastCommented = '';
          this.commentListLength--;

        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });
  }
  goToProfile(username) {
    //console.log(username);
    // this.router.navigateByUrl('profileview/'+username)
    this.router.navigate(['profileview', username])
  }

  postComments(postId) {
    const element = this.elRef.nativeElement.querySelector('textarea.mainComment');
    let comment = element.value;
    //console.log(comment);


    let file = '';
    let fileType = '';
    let commentType = '';

    if ((postId == this.selectedPostId) && DetailedPostComponent.temp.uploadFileType != '') {
      file = DetailedPostComponent.temp.uploadFileBase64
      fileType = DetailedPostComponent.temp.uploadFileType
      commentType = DetailedPostComponent.temp.commentType
    } else {
      file = '';
      fileType = '';
      commentType = 'text';
    }

    const rqstData = JSON.stringify(
      {
        postId: postId,
        comment: comment,
        commentType: commentType,
        fileData: file,
        fileType: fileType
      }
    );


    this.isLoadingComment = true;
    //console.log(rqstData);
    const result = this.callService(ApiUrls.commentPost, rqstData).subscribe((json: Object) => {
      this.postComment = new PostComment().fromJSON(json);
      //console.log(this.postComment);
      this.commentListLength = this.commentListLength + 1;
      this.walpostList.comment_count = this.commentListLength;
      this.child.getComments(postId);

      this.isLoadingComment = false;
      element.value = '';
      this.toggleCommented = false;
      if (this.postComment.status === 'Success') {
        // location.reload();

        const commentOut_a = {
          postId: postId,
          commentType: 'new comment'
        }

        this.commentFnOutput.emit(commentOut_a);
        file = '';
        fileType = '';
        commentType = '';
        DetailedPostComponent.temp.uploadFileBase64 = '';
        DetailedPostComponent.temp.uploadFileType = '';
        DetailedPostComponent.temp.commentType = '';
        this.selectedPostId = '';
        this.getComments();

        ////////// not getting here
        // this.child.getComments(postId);
        // this.getComments();
        return;
      } else {
        this.zone.run(() => {
          this.router.navigateByUrl('login');
        });
      }

    }, error => {
      this.errorMessage = <any>error;
    });

  }

  uploadFileShowing(event) {
    //console.clear();
    //console.log(event);
    const file_a = event.srcElement.files[0];
    this.selectedPostId = event.srcElement.name;
    ////console.log(file_a);
    const fileName_str = file_a.name;
    const fileType_str = file_a.type;
    let lastIndex = fileType_str.lastIndexOf('/');
    lastIndex++;
    const baseTpeLength = fileType_str.length;
    const fileType = fileType_str.substring(lastIndex, baseTpeLength);

    lastIndex--;
    const commentType = fileType_str.substring(0, lastIndex);

    this.getBase64(file_a, this.saveFileArray, fileType, commentType);
    this.uploadFile = this.elRef.nativeElement.querySelector('input[type="file"]').value;
    this.uploadFilename = this.uploadFile.replace(/.*[\/\\]/, '');
    if (this.uploadFilename != '') {
      this.isHasContent = true;
    }
  }

  removeFile() {
    this.elRef.nativeElement.querySelector('input[type="file"]').value = '';
    this.isHasContent = false;
    DetailedPostComponent.temp.uploadFileBase64 = '';
    DetailedPostComponent.temp.uploadFileType = '';
    DetailedPostComponent.temp.commentType = '';
    this.uploadFilename = '';
  }


  getBase64(file, callback, fileType, commentType) {
    //console.log(fileType);
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      let base64Value = reader.result;
      let lastIndex = base64Value.lastIndexOf(',');
      lastIndex++;
      const base64Length = base64Value.length;
      const converted64 = base64Value.substring(lastIndex, base64Length);
      //console.log(converted64);
      //console.log(commentType);


      callback(converted64, fileType, commentType);
    };
    reader.onerror = function (error) {
      //console.log('Error: ', error);
    };
  }

  saveFileArray(base64, fileType, commentType) {
    DetailedPostComponent.temp.uploadFileBase64 = base64;
    DetailedPostComponent.temp.uploadFileType = fileType;
    DetailedPostComponent.temp.commentType = commentType;
  }


  /**
   * api calling
   */
  callService(url, userdata): Observable<Response[]> {
    let headers = new Headers(
      {
        'Content-Type': 'application/json',
        'token': this.access_token
      }
    );
    let options = new RequestOptions({ headers: headers });

    //console.log(userdata);
    return this.http.post(url, userdata, options)
      .map((res: Response) => res.json()
      )
      .catch(this.handleErrorObservable);
  }

  private handleErrorObservable(error: Response | any) {
    //console.log("Error");
    if (error.status == 401) {
      window.location.href = "./#/login";
    }
    return Observable.throw(JSON.parse(error._body).errorMessage || error);
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json'
    };
    return new Headers(headersConfig);
  }

}
