export class ActivateProfile {
	public status: string;
	public message: string;
	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
