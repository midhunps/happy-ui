export class Search {
	public status: string;
	public message: string;
	public profile_search: {
    	profile_count:number;
    	profile_data: Array<{
    		full_name: string;
    		id: number;
    		dob: string;
    		address: string;
    		email: string;
    		username: string;
    		gender: string;
    		contact: string;
    		profile_photo: string;
    	}>; 
    };
	public post_search: {
    	post_count:number;
    	post_data: Array<{
    		full_name: string;
    		id: number;
    		follow_count: string;
    		attachment: string;
    		comment_count: string;
    		username: string;
    		like_count: string;
    		post_type: string;
    		category_name: string;
    		categroy_id: string;
    		post: string;
    		contact: string;
    		profile_photo: string;
    	}>; 
    };
	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
