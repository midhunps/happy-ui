export class Notification {
	public status: string;
	public message: string;
	public unread_count: string;
	public data: Array<{
    	full_name:string, 
    	username:string, 
    	notification_content:string, 
    	notification_type:string, 
    	is_read:string,
    	date:string,
    	profile_photo:string,
    	notification_id:string,
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
