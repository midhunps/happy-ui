export class ListComment {
	public status: string;
	public message: string;
	public data: Array<{
    	id:string,  
    	comment:string,  
    	full_name:string,  
    	profile_photo:string,  
    	comment_date:string,  
    	own_comment:string,  
    	post_id:string,  
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
