export class PostLikes {
	public status: string;
	public message: string;
	public data: Array<{
    	profile_photo:string, 
    	full_name:string, 
    	date:string, 
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
