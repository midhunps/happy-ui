export class WebsiteData {
    public status: string;
    public message: string;
    public data: Array<{
        title: string,
        icon: string,
        link: string,
        images: Array<{}>
    }>;

    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}
