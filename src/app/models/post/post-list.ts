export class PostList {
    public status: string;
	public message: string;
	public count: number;
	public data: Array<{
    	id:number, 
    	post:string, 
    	categroy_id:number, 
    	category_name:string, 
    	post_type:string, 
    	like_count:number, 
    	comment_count:number, 
    	follow_count:number, 
    	attachment:string, 
    	profile_photo:string, 
        full_name:string, 
        username:string, 
		own_post: string, 
		web_title: string,
		web_icon: string,
		web_link: string,
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
