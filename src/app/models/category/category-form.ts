export class CategoryForm {
	public status: string;
	public message: string;
	public data: Array<{
    	id:number, 
    	form_value_name:string
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
