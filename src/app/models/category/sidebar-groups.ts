export class SidebarGroups {
	public status: string;
	public message: string;
	public data: Array<{
    	id:number,  
		category_name:string, 
		group_id:string, 
		group_name:string, 
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
