export class CategoryInfo {
	public status: string;
	public message: string;
	public data: Array<{
    	logo:string
    	category_name:string
    	description:string
		have_form:string
		category_type:string
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
