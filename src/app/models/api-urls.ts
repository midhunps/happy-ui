export class ApiUrls {

	public static get baseUrl (): string { return 'https://happyequities.com/head/index.php/api/'};
	public static get mediaUrl (): string { return 'https://happyequities.com/head/'}; 
	// public static get baseUrl(): string { return 'https://rth454.online/happy/index.php/api/' };
	// public static get mediaUrl(): string { return 'https://rth454.online/happy/' };
	// public static get baseUrl(): string { return 'http://localhost/happy/index.php/api/' };
	// public static get mediaUrl(): string { return 'http://localhost/happy/' };

	public static get loginUrl(): string { return this.baseUrl + 'login' };
	public static get signup(): string { return this.baseUrl + 'signup' };
	public static get activateProfile(): string { return this.baseUrl + 'activate/' }
	public static get forgotPassword(): string { return this.baseUrl + 'forgotPassword' }
	public static get resetPassword(): string { return this.baseUrl + 'resetPassword' }

	public static get ownProfile(): string { return this.baseUrl + 'profile' };
	public static get postCategory(): string { return this.baseUrl + 'postCategory' };
	public static get changePassword(): string { return this.baseUrl + 'changePassword' };
	public static get editProfile(): string { return this.baseUrl + 'editProfile' };
	public static get inviteFriend(): string { return this.baseUrl + 'inviteFriend' };

	public static get sidemenuCategories(): string { return this.baseUrl + 'sidemenuCategories' };
	public static get getCategoryForm(): string { return this.baseUrl + 'getCategoryForm' };
	public static get categoryFormMail(): string { return this.baseUrl + 'categoryFormMail' };
	public static get getCategoryInfo(): string { return this.baseUrl + 'getCategoryInfo' };
	public static get sideMenuGroups(): string { return this.baseUrl + 'sideMenuGroups' };
	public static get sideMenuCategoriesWithGroup(): string { return this.baseUrl + 'sideMenuCategoriesWithGroup' };


	public static get createPost(): string { return this.baseUrl + 'createPost' };
	public static get postList(): string { return this.baseUrl + 'getPostList' };
	public static get getPostDetails(): string { return this.baseUrl + 'getPostDetails' };
	public static get getLikes(): string { return this.baseUrl + 'getLikes' };
	public static get likePost(): string { return this.baseUrl + 'likePost' };
	public static get followPost(): string { return this.baseUrl + 'followPost' };
	public static get ownPosts(): string { return this.baseUrl + 'ownPosts' };
	public static get otherProfile(): string { return this.baseUrl + 'otherProfile' };
	public static get categoryBasedPosts(): string { return this.baseUrl + 'categoryBasedPosts' };
	public static get deletePost(): string { return this.baseUrl + 'deletePost' };
	public static get updatePost(): string { return this.baseUrl + 'updatePost' };
	public static get followedPosts(): string { return this.baseUrl + 'followedPosts' };

	public static get commentPost(): string { return this.baseUrl + 'commentPost' };
	public static get getComments(): string { return this.baseUrl + 'getComments' };
	public static get deleteComment(): string { return this.baseUrl + 'deleteComment' };
	public static get editComment(): string { return this.baseUrl + 'editComment' };

	public static get search(): string { return this.baseUrl + 'search' };

	public static get getNotificationList(): string { return this.baseUrl + 'getNotificationList' };
	public static get readNotification(): string { return this.baseUrl + 'readNotification' };
	public static get markAllRead(): string { return this.baseUrl + 'markAllRead' };

	public static get getAdvertisements(): string { return this.baseUrl + 'getAdvertisements' };

	public static get fetchWebsiteDetails(): string { return this.baseUrl + 'fetchWebsiteDetails' };
	public static get enquiry(): string { return this.baseUrl + 'enquiry' };

}
