export class OwnDetails {
	public status: string;
	public data: Array<{
    	first_name:string, 
    	last_name:string, 
    	dob:string, 
    	address:string, 
    	email:string, 
    	username:string, 
    	gender:string, 
    	contact:string, 
		profile_photo:string, 
		homepage_post: string,
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
