import { Injectable } from '@angular/core';
@Injectable()
export class Global {
	profilePic: string = '';
	profileName: string = '';
	activatingEmail: string = '';

	/**
	 * category page
	 */
	
	categoryPostList: any;
	categoryId: string = '';
	categoryInfo: any;
	categoryForm: any;
	categoryPostCheck: any;
	catIvalid: boolean = true;

	/**
	 * detailed posts
	 */

	detailedPostId: number;
	detailedPostIndex: number;
	
	homepage_post: string;

	/**
	 * local timezone
	 */

	timezone: string;
}
