export class Advertisement {
	public status: string;
	public message: string;
	public data: Array<{
    	id:string, 
    	title:string, 
    	attachment:string, 
    	description:string
    }>;

	fromJSON(json) {
	        for (var propName in json)
	            this[propName] = json[propName];
	        return this;
	    }
}
